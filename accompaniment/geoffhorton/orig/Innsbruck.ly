% Version 1.1
% Last edit: March 24, 2006
% The music and words produced by this source code are believed
% to be in the public domain in the United States. The source
% code itself is covered by the Creative Commons Attribution-
% NonCommercial license, 
% http://creativecommons.org/licenses/by-nc/2.5/
% Attribution: Geoff Horton

\version "2.7.38"
\include "english.ly"

#(set-default-paper-size "letter")
#(set-global-staff-size 20)

title = "The Duteous Day Now Closeth"
composer = "Traditional German Melody"
poet = "Paulus Gerhardy"
piece = "Innsbruck"
meter = "776.778"
arranger = "harm. J. S. Bach"
translator = "Para. Robert Bridges"


world = {
  \key f \major
  \time 4/2
  \partial 2*1
}

melody = \relative c'' {
  \world
  a2 |
  f g a4( bf) c2 |
  bf1 a2 a4( bf) |
  c2 c g a |
  f1 e2 c |
  f g a a |
  g1. a2 |
  f g a4( bf) c2 |
  bf1 a2 a4( bf) |
  c2 c g a |
  f1 e2 c |
  f g a bf |
  a g f1 \bar "|."
}


alto = \relative c' {
  \world
  f2 |
  d e f4( g) a2 |
  a( g) f f |
  f4( e) f( d) e( d) c2 |
  c( b) c g |
  c bf a4( bf) c2 |
  c1. e2 |
  d c c d |
  d4( c d e) fs2 f |
  g f4( e) d2 e |
  e( d) cs c |
  c4( d) d( bf) a( c) d( e) |
  f2 e2 c1
}

tenor = \relative c' {
  \world
  c2 |
  bf bf c4( bf) a( g) |
  f2( c') c d |
  c c c e, |
  f4( e) f2 g e4( d) |
  c2 d4( e) f2 f |
  e1. a2 |
  a4( bf) a( g) f( g) a2 |
  g4( a bf c) d2 d |
  c4( bf) a2 b e, |
  a1 a2 a |
  a4( bf) a( g) f2 f |
  d' g,4( a8[ bf]) a1 \bar "|."
}

bass = \relative c {
  \world
  f2 |
  bf4( a) g2 f2. e4 |
  d2( e) f d |
  a4( g) a( bf) c( bf) a2 |
  d1 c2 bf |
  a g f4( g) a( bf) |
  c1. cs2 |
  d e f fs |
  g( g,) d' d |
  e f f4( e) d( cs) |
  d( e f g) a2 a, |
  d e f4( e) d( c) |
  bf2 c < \tweak #'font-size #-1 f f, >1
}

verseOne = \lyricmode {
  \set stanza = "1. "
  The du -- teous day now clos -- eth,
  Each flow'r and tree re -- pos -- eth,
  Shade creeps o'er wild and wood;
  Let us, as night is fall -- ing,
  On God our Mak -- er call -- ing,
  Give thanks to him, the Giv -- er good.
}

verseTwo = \lyricmode {
  \set stanza = "2. "
  Now all the heav'n -- ly splen -- dor
  Breaks forth in star -- light ten -- der
  From myr -- iad worlds un -- know;
  And man, the mar -- vel see -- ing,
  For -- gets his self -- ish be -- ing,
  For joy of beau -- ty not his own.
}

verseThree = \lyricmode {
  \set stanza = "3. "
  A -- while his mor -- tal blind -- ness
  May miss God's lov -- ing kind -- ness,
  And grope in faith -- less strife:
  But when life's day is o -- ver
  Shall death's fair night dis -- cov -- er
  The fields of ev -- er -- last -- ing life.
}

\markup {
  \column {
    \fill-line { \large \bold \title } % title
    \fill-line { \caps \piece               % piece
	         \caps \composer      % composer
	  }
    \fill-line { \meter          % meter
	         \arranger           % arranger
	  }
  }
}

\score {
  \context ChoirStaff <<
    \context Staff = upper <<
      \context Voice =
         sopranos { \voiceOne << \melody >> }
      \context Voice =
         altos { \voiceTwo << \alto >> }
      \context Lyrics = one \lyricsto sopranos \verseOne
      \context Lyrics = two \lyricsto sopranos \verseTwo
      \context Lyrics = three \lyricsto sopranos \verseThree
    >>
    \context Staff = lower <<
      \clef bass
      \context Voice =
        tenors { \voiceOne << \tenor >> }
      \context Voice =
        basses { \voiceTwo << \bass >> }
    >>
  >>
  \layout {
    \context {
      \Score
      % **** Turns off bar numbering
      \remove "Bar_number_engraver"
    }
    \context {
      \Lyrics
      % **** Prevents lyrics from running too close together
      \override LyricSpace #'minimum-distance = #0.6
      % **** Makes the text of lyrics a little smaller
      \override LyricText #'font-size = #-1
      % **** Moves lines of lyrics closer together
      \override VerticalAxisGroup #'minimum-Y-extent = #'(-1 . 1)
    }
  }
}

\markup {
  \fill-line {
    " "
    \column {
      \small \caps \poet % poet
      \small \caps \translator % translator
    }
  }
}


\paper {
  ragged-bottom = ##t
  top-margin = 0.25\in
  bottom-margin = 0.25\in
}

%{
  Per _The Hymnal 1940_, 181.
  Change log:
  3-24-06 convert to 2.8 and current formatting
%}

