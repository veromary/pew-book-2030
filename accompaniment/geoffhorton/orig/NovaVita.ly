% Version 1.10
% Last edit: March 24, 2006
% The music and words produced by this source code are believed
% to be in the public domain in the United States. The source
% code itself is covered by the Creative Commons Attribution-
% NonCommercial license, 
% http://creativecommons.org/licenses/by-nc/2.5/
% Attribution: Geoff Horton

\version "2.8.0"
\include "english.ly"

#(set-default-paper-size "letter")
#(set-global-staff-size 20)

title = "Breathe on Me, Breath of God"
composer = "Lister R. Peace"
poet = "Edwin Hatch"
piece = "Nova Vita"
meter = "S.M."
arranger = ""

world = {
  \key d \major
  \time 3/4
}

melody = \relative c' {
  \world
  fs4 fs fs |
  fs2 e4 |
  e2. |
  a4 a a |
  a2 gs4 |
  gs2. |
  cs4 cs cs |
  d2 cs4 |
  cs2 b4 |
  a2 g4 |
  fs( a) g |
  fs2 e4 |
  d2. \bar "|."
}


alto = \relative c' {
  \world
  d4 d d |
  d2 d4 |
  cs2. |
  fs4 fs e |
  d2 e4 |
  e2. |
  e4 e e |
  d( fs) a |
  a2 g4 |
  fs2 e4 |
  d2 d4 |
  d2 cs4 |
  d2.
}

tenor = \relative c' {
  \world
  a4 a a |
  b2 b4 |
  a2. |
  cs4 cs cs |
  b2 b4 |
  b2. |
  a4 a a |
  a2 d4 |
  d2 d4 |
  c2 b4 |
  a( b) b |
  a2 g4 |
  fs2.
}

bass = \relative c {
  \world
  d4 d fs, |
  g2 g4 |
  a2. |
  fs4 fs' fs |
  b,2 e4 |
  e2. |
  a,4 a' g |
  fs2 fs4 |
  g2 g4 |
  ds2 e4 |
  a( e) e |
  a,2 a4 |
  d2.
}

verseOne = \lyricmode {
  \set stanza = "1. "
  Breathe on me, Breath of God,
  Fill me with life a -- new,
  That I may love what thou dost love,
  And do what thou wouldst do.
}

verseTwo = \lyricmode {
  \set stanza = "2. "
  Breathe on me, Breath of God,
  Un -- til my heart is pure,
  Un -- til with thee I will one will,
  To do or to en -- dure.
}

\markup {
  \column {
    \fill-line { \large \bold \title } % title
    \fill-line { \caps \piece               % piece
	         \caps \composer      % composer
	  }
    \fill-line { \meter          % meter
	         \arranger           % arranger
	  }
  }
}

\score {
  \context ChoirStaff <<
    \context Staff = upper <<
      \context Voice =
         sopranos { \voiceOne << \melody >> }
      \context Voice =
         altos { \voiceTwo << \alto >> }
      \context Lyrics = one \lyricsto sopranos \verseOne
      \context Lyrics = two \lyricsto sopranos \verseTwo
    >>
    \context Staff = lower <<
      \clef bass
      \context Voice =
        tenors { \voiceOne << \tenor >> }
      \context Voice =
        basses { \voiceTwo << \bass >> }
    >>
  >>
  \layout {
    \context {
      \Score
      % **** Turns off bar numbering
      \remove "Bar_number_engraver"
    }
    \context {
      \Lyrics
      % **** Prevents lyrics from running too close together
      \override LyricSpace #'minimum-distance = #0.6
      % **** Makes the text of lyrics a little smaller
      \override LyricText #'font-size = #-1
      % **** Moves lines of lyrics closer together
      \override VerticalAxisGroup #'minimum-Y-extent = #'(-1 . 1)
    }
  }
}

\markup { 
  \small {
    \fill-line {
      \hspace #3.0
      \line {
	"3. "
	\column {
	  "Breathe on me, Breath of God,"
	  "  Till I am wholly thine,"
	  "Till all this earthly part of me"
	  "  Glows with thy fire divine."
	}
      }
      \hspace #3.0
      \line {
	"4. "
	\column {
	  "Breathe on me, Breath of God,"
	  "  So shall I never die;"
	  "But live with thee the perfect life"
	  "  Of thine eternity."
	}
      }
      \hspace #3.0
    }
  }
}

\markup {
  \fill-line {
    " "
    \column {
      \small \caps \poet % poet
    }
  }
}


%{
  Per the _Hymnal 1940_ (375, 2nd tune)
  Change log:
  3-24-06 Move to 2.8 and current formatting
%}


