% Version 1.0
% Last edit: April 4, 2006
% The music and words produced by this source code are believed
% to be in the public domain in the United States. The source
% code itself is covered by the Creative Commons Attribution-
% NonCommercial license, 
% http://creativecommons.org/licenses/by-nc/2.5/
% Attribution: Geoff Horton

\version "2.8.0"
\include "english.ly"

#(set-default-paper-size "letter")
#(set-global-staff-size 20)
#(ly:set-option (quote no-point-and-click))

title = "Come, Ye Faithful, Raise the Strain"
composer = "Arthur Seymour Sullivan"
poet = "St. John of Damascus"
translator = "Tr. John Mason Neale"
meter = "76.76.D."
arranger = ""
piece = "St. Kevin"

world = {
  \key f \major
  \time 4/4
}

melody = \relative c'' {
  \world
  \autoBeamOff 
  a4 a a8[ g] f4 |
  a4 bf c2 |
  f,4 g a bf |
  a2 g | % end line 1
  g4 g b8[ a] g4 |
  c4 c e,2 |
  g4 g a g8[ f] |
  e2 c | % end line 2
  g'4 g g8[ f] g4 |
  a g a2 |
  a4 a bf8[ c] d4 |
  f,2 e | % end line 3
  a4 a a8[ g] f4 |
  a bf c2 |
  f,4 g a bf |
  g2 f \bar "|."
}


alto = \relative c' {
  \world
  f4 f f8[ e] f4 |
  f e f2 |
  d4 e f g |
  f2 e | % end line 1
  f4 f d d |
  c c c2 |
  e4 e f e8[ d] |
  c2 c | % end line 2
  e4 e e8[ d] c4 |
  c e f2 |
  fs4 fs g d |
  c2 c | % end line 3
  f4 f f8[ e] f4 |
  f f f2 |
  f4 f f f |
  e2 f 
}

tenor = \relative c' {
  \world
  c4 c c8[ bf] a4 |
  c c c2 |
  a4 c c d |
  c2 c | % end line 1
  b4 b g8[ a] b4 |
  c g c2 |
  c4 c b b |
  c2 c, | % end line 2
  bf'4 bf bf8[ a] g4 |
  f c' c2 |
  d4 d d8[ c] bf4 |
  a2 g | % end line 3
  c4 c c8[ bf] a4 |
  c c ef2 |
  d4 d c bf |
  bf2 a \bar "|."
  
}

bass = \relative c {
  \world
  f4 f f f |
  f g a2 |
  d,4 c f bf, |
  c2 c | % end line 1
  d4 d f f |
  e e a2 |
  g4 g g, g |
  c2 c | % end line 2
  c4 c c8[ d] e4 |
  f c f2 |
  d4 d g bf, |
  c2 c | % end line 3
  f4 f f f |
  f g a2 |
  bf4 bf a g |
  c,2 f
}

verseOne = \lyricmode {
  \set stanza = "1. "
  Come, ye faith -- ful, raise the strain
  Of tri -- umph -- ant glad -- ness;
  God hath brought his Is -- ra -- el
  In -- to joy from sad -- ness;
  Loosed from Pha -- roah's bit -- ter yoke
  Ja -- cob's sons and daugh -- ters;
  Led them with un -- mois -- tened foot
  Through the Red Sea wa -- ters.
}

verseTwo = \lyricmode {
  \set stanza = "2. "
  'Tis the spring of souls to -- day;
  Christ hath burst his pris -- on,
  And from three days' sleep in death
  As a sun hath ri -- sen;
  All the win -- ter of our sins,
  Long and dark, is fly -- ing
  From his light, to whom we give
  Laud and praise un -- dy -- ing.
}

\markup {
  \column {
    \fill-line { \large \bold \title } % title
    \fill-line { \caps \piece               % piece
	         \caps \composer      % composer
	  }
    \fill-line { \meter          % meter
	         \arranger           % arranger
	  }
  }
}

\score {
  \context ChoirStaff <<
    \context Staff = upper <<
      \context Voice =
         sopranos { \voiceOne << \melody >> }
      \context Voice =
         altos { \voiceTwo << \alto >> }
      \context Lyrics = one \lyricsto sopranos \verseOne
      \context Lyrics = two \lyricsto sopranos \verseTwo
    >>
    \context Staff = lower <<
      \clef bass
      \context Voice =
        tenors { \voiceOne << \tenor >> }
      \context Voice =
        basses { \voiceTwo << \bass >> }
    >>
>>
  \layout {
    \context {
      \Score
      % **** Turns off bar numbering
      \remove "Bar_number_engraver"
    }
    \context {
      \Lyrics
      % **** Prevents lyrics from running too close together
      \override LyricSpace #'minimum-distance = #0.6
      % **** Makes the text of lyrics a little smaller
      \override LyricText #'font-size = #-1
      % **** Moves lines of lyrics closer together
      \override VerticalAxisGroup #'minimum-Y-extent = #'(-1 . 1)
    }
  }
}


\markup { 
  \normalsize {
    \fill-line {
      \hspace #3.0
      \line {
	\bold "3. "
	\column {
	  "Now the queen of seasons, bright"
	  "  With the day of splendor,"
	  "With the royal feast of feasts,"
	  "  Comes its joy to render;"
	  "Comes to glad Jerusalem,"
	  "  Who with true affection"
	  "Welcomes in unwearied strains"
	  "Jesus' resurrection."
	}
      }
      \hspace #3.0
      \line {
	\bold "4. "
	\column {
	  "Neither might the gates of death,"
	  "  Nor the tomb's dark portal,"
	  "Nor the watchers, nor the seal"
	  "  Hold thee as a mortal:"
	  "But to-day amidst thine own"
	  "  Thou didst stand, bestowing"
	  "That thy peace which evermore"
	  "  Passeth human knowing."
	}
      }
      \hspace #3.0
    }
  }
}

\markup {
  \fill-line {
    " "
    \column {
      \small \caps \poet % poet
      \small \caps \translator % translator
    }
  }
}



\paper {
  ragged-bottom = ##t
  top-margin = 0.25\in
  bottom-margin = 0.25\in
  head-separation = 0\in
}

%{
  _The Hymnal 1940_, #94, second tune.
  Change history
  4/4/06 Original version
%}


