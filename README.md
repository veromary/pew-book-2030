# Pew Book 2030

First there was **The Mass Book** of 2012. The cover was beige with an excruciating crucifixion scene on the back.

Well, that wasn't really the first one, but it's the one which made it to the printers. There was a home printed version before that, bound with brown paper and red cloth tape with a different holy card or calendar cutting on the front. And there have been many interim spin offs. But let's keep it simple.

Then in 2022 we updated the book with a green **Missal & Hymnal**. It was great. But there are always things which we would like to improve.

The first major difference would be to prepare melody lines and accompaniments for each the 212 hymns.

### Accompaniment

We use Benjamin Bloomfield's [Songs from the Public Domain](http://bbloomf.github.io/lilypond-songs/) and [Christmas Carols](https://acollectionofchristmascarols.com/) as a basis.

There are also other collections of Lilypond hymns which would be handy in incorporate, first adjusting the lyrics and then matching the style later.

 * The English Hymnal - which has been a Project Gutenberg Project in the past - so Public Domain
 * [Geoff Horton's Library](http://geoffhorton.com/hymns/library.html) his work is licensed under a Creative Commons Attribution-NonCommercial 2.5 License.
 * [Mutopia](https://www.mutopiaproject.org/cgibin/make-table.cgi?Style=Hymn)
 * [CPDL](https://cpdl.org/wiki/)
 * [HymnWiki](https://www.hymnwiki.org/Main_Page) - public domain
 * [Magdalene Sacred Music](https://github.com/magdalenesacredmusic/English-Hymns)

### Melody

I guess this is just for the modern music.

Some musicians appreciate having chord names in simple arrangements with the melody.

### Chant

Lots of chant. We want to normalise chant, or even boost it back to its place of prominence in the congregation.

### Copyright

There are many public domain hymns to start on.

The copyright hymns which we would like are:

 * James McAuley's *Help of Christians*
 * Richard Connolly's *Holy Father, God of Might* (now administered by Willow Publishing)
 * a variety of Ronald Knox hymns, which enter the public domain in 2027
 * Fr David Coffey's *Receive O Father, God of Might* (Faber Music)

but these are not essential.


