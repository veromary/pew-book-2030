% Version 1.20
% Last edit: March 22, 2006

\version "2.8.0"
\include "english.ly"

#(set-default-paper-size "letter")
#(set-global-staff-size 20)

title = "When Morning Gilds the Skies"
composer = "Joseph Barnaby"
poet = "German, anon."
translator = "trans. Robert Bridges"
piece = "Laudes Domini"
meter = "66.66.66"
arranger = ""

world = {
  \key c \major
  \time 4/4
  \partial 4*1
}

melody = \relative c' {
  \world
  e4 |
  f g a c |
  b2( a4) g |
  a b c e |
  d2. c4 |
  b c a4. g8 |
  g2. g4 |
  g c b c |
  g2. g4 |
  g c b c |
  g2. g4 |
  g2 c |
  c d |
  c2. \bar "|."
}


alto = \relative c' {
  \world
  c4 |
  c c c f |
  f2. f4 |
  e g g g |
  fs2( g4) d |
  d4 e c4. b8 |
  b2. d4 |
  c c f f |
  f2( e4) d |
  c e fs fs |
  g( d e) f |
  e( f) g2 |
  f2 f |
  e2. 
}

tenor = \relative c' {
  \world
  g4 |
  g g f a |
  b2( c4) d |
  c d c c |
  c2( b4) a |
  g g g fs |
  g2. b4 |
  c g b a |
  d( c b) b |
  c g d' c |
  b2. b4 |
  c( d) e2 |
  c2. b!4 |
  c2.
}

bass = \relative c {
  \world
  c4 |
  d e f d |
  g2( a4) b |
  c b a a |
  d,2( e4) fs |
  g c, d d |
  g,2. f'!4 |
  e e d c |
  b( a g) f' |
  e c d d |
  g( f e) d |
  c2 <bf' \tweak #'font-size #-2 bf,> |
  <a \tweak #'font-size #-2 a,> <g \tweak #'font-size #-2 g,> |
  <g c,>2.
}

verseOne = \lyricmode {
  \set stanza = "1. "
  When morn -- ing gilds the skies,
  My heart, a -- wak -- ing, cries,
  May Je -- sus Christ be praised!
  When eve -- ning sha -- dows fall, 
  This rings my cur -- few call,
  May Je -- sus Christ be praised!
}

verseTwo = \lyricmode {
  \set stanza = "2. "
  When mirth for mu -- sic longs,
  This is my song of songs:
  May Je -- sus Christ be praised!
  God's ho -- ly house of pray'r
  Hath none that can com -- pare
  With: Je -- sus Christ be praised!
}

verseThree = \lyricmode {
  \set stanza = "3. "
  No love -- lier an -- ti -- phon
  In all high heav'n is known
  Than, Je -- sus Christ be praised!
  There to the-e -- ter -- nal Word
  The-e -- ter -- nal psalm is heard:
  May Je -- sus Christ be praised!
}

\markup {
  \column {
    \fill-line { \large \bold \title } % title
    \fill-line { \caps \piece               % piece
	         \caps \composer      % composer
	  }
    \fill-line { \meter          % meter
	         \arranger           % arranger
	  }
  }
}

\score {
  \context ChoirStaff <<
    \context Staff = upper <<
      \context Voice =
         sopranos { \voiceOne << \melody >> }
      \context Voice =
         altos { \voiceTwo << \alto >> }
      \context Lyrics = one \lyricsto sopranos \verseOne
      \context Lyrics = two \lyricsto sopranos \verseTwo
      \context Lyrics = three \lyricsto sopranos \verseThree
    >>
    \context Staff = lower <<
      \clef bass
      \context Voice =
        tenors { \voiceOne << \tenor >> }
      \context Voice =
        basses { \voiceTwo << \bass >> }
    >>
>>
  \layout {
    \context {
      \Score
      % **** Turns off bar numbering
      \remove "Bar_number_engraver"
    }
    \context {
      \Lyrics
      % **** Prevents lyrics from running too close together
      \override LyricSpace #'minimum-distance = #0.6
      % **** Makes the text of lyrics a little smaller
      \override LyricText #'font-size = #-1
      % **** Moves lines of lyrics closer together
      \override VerticalAxisGroup #'minimum-Y-extent = #'(-1 . 1)
    }
  }
}

\markup { 
  \normalsize {
    \fill-line {
      \hspace #3.0
      \line {
	"4. "
	\column {
	  "Ye nations of mankind,"
	  "In this your concord find:"
	  "  May Jesus Christ be praised!"
	  "Let all the earth around"
	  "Ring joyous with the sound:"
	  "  May Jesus Christ be praised!"
	}
      }
      \hspace #3.0
      \line {
	"5. "
	\column {
	  "Sing, suns and stars of space,"
	  "Sing, ye that see his face,"
	  "  Sing, Jesus Christ be praised!"
	  "God's whole creation o'er,"
	  "For aye and evermore"
	  "  Shall Jesus Christ be praised!"
	}
      }
      \hspace #3.0
    }
  }
}

\markup {
  \fill-line {
    " "
    \column {
      \small \caps \poet % poet
      \small \caps \translator % translator
    }
  }
}

%{
  Per the _Hymnal 1940_, 367
  Change log: 
  3-24-06, convert to 2.8 and current formatting
%}


