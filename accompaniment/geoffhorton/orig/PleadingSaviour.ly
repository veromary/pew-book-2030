% Version 1.20
% Last edit: March 24, 2006
% The music and words produced by this source code are believed
% to be in the public domain in the United States. The source
% code itself is covered by the Creative Commons Attribution-
% NonCommercial license, 
% http://creativecommons.org/licenses/by-nc/2.5/
% Attribution: Geoff Horton

\version "2.8.0"
\include "english.ly"

#(set-default-paper-size "letter")
#(set-global-staff-size 20)

title = "Sing of Mary"
composer = \markup { \italic "Plymouth Collection"}
poet = "Anonymous"
piece = "Pleading Saviour"
meter = "87.87. D."
arranger = ""

world = {
  \key f \major
  \time 2/4
  \autoBeamOff
}

melody = \relative c'' {
  \world
  a4 g8[ f] |
  d4 f8[ g] |
  a4 c |
  a g |
  a g8[ f] |
  d4 f8[ g] |
  a4 g |
  f2 |
  a4 g8[ f] |
  d4 f8[ g] |
  a4 c |
  a g |
  a g8[ f] |
  d4 f8[ g] | 
  a4 g | 
  f2 |
  c'4 c8[ a] |
  c4 c |
  d c8[ a] |
  a4 g |
  c c8[ a] |
  c4 c |
  d c8[ a] |
  g2 |
  a4 g8[ f] |
  d4 f8[ g] |
  a4 c |
  a g | 
  a g8[ f] |
  d4 f8[ g] |
  a4 g |
  f2 \bar "|."
}


alto = \relative c' {
  \world
  c4 c |
  d c8[ e] |
  f4 f |
  f e |
  c c |
  d c8[ f] |
  f4 e |
  f2 |
  c4 c |
  d c8[ e] |
  f4 f |
  f e |
  c c |
  d c8[ f] |
  f4 e |
  f2 |
  f4 f |
  f f |
  f f |
  f e |
  f f |
  f f |
  d f |
  f( e) |
  c4 c |
  d c8[ e] |
  f4 f |
  f e |
  c c |
  d c8[ f] |
  f4 e |
  f2
}

tenor = \relative c' {
  \world
  c4 bf8[ a] |
  f4 a8[ bf] |
  c4 c | d bf |
  a bf8[ a] |
  bf4 c8[ d] |
  c4 c8[ bf] |
  a2 |
  c4 bf8[ a] |
  f4 a8[ bf] |
  c4 c |
  d bf |
  a bf8[ a] |
  bf4 c8[ d] |
  c4 c8[ bf] |
  a2 |
  c4 c |
  c c |
  bf c |
  c c |
  c c |
  c c | 
  bf c8[ d] |
  g,4( c) |
  c4 bf8[ a] |
  f4 a8[ bf] |
  c4 c |
  d bf |
  a bf8[ a] |
  bf4 c8[ d] |
  c4 c8[ bf] |
  a2
}

bass = \relative c {
  \world
  f4 e8[ f] |
  bf,4 a8[ g] |
  f4 a |
  bf c | 
  f e8[ f] |
  bf,4 a8[ bf] |
  c4 c |
  f2 |
  f4 e8[ f] |
  bf,4 a8[ g] |
  f4 a |
  bf c |
  f e8[ f] |
  bf,4 a8[ bf] |
  c4 c |
  f2 |
  a4 a8[ f] |
  a4 a |
  bf a8[ f] |
  c4 c |
  a'4 a8[ f] |
  a4 a, |
  bf a8[ bf] |
  c2 |
  f4 e8[ f] |
  bf,4 a8[ g] |
  f4 a |
  bf c |
  f e8[ f] |
  bf,4 a8[ bf] |
  c4 c |
  f2
}

verseOne = \lyricmode {
  \set stanza = "1. "
  Sing of Ma -- ry, pure and low -- ly, 
  Vir -- gin -- moth -- er un -- de -- filed.
  Sing of God's own Son most ho -- ly,
  Who be -- came her lit -- tle child.
  Fair -- est child of fair -- est moth -- er,
  God the Lord who came to earth,
  Word made flesh, our ve -- ry broth -- er,
  Takes our na -- ture by his birth.
}

verseTwo = \lyricmode {
  \set stanza = "2. "
  Sing of Je -- sus, son of Ma -- ry,
  In the home at Na -- za -- reth,
  Toil and la -- bor can -- not wea -- ry
  Love en -- dur -- ing un -- to death.
  Con -- stant was the love he gave her,
  Though he went forth from her side,
  Forth to preach, and heal, and suf -- fer,
  Till on Cal -- va -- ry he died.
}

\markup {
  \column {
    \fill-line { \large \bold \title } % title
    \fill-line { \caps \piece               % piece
	         \caps \composer      % composer
	  }
    \fill-line { \meter          % meter
	         \arranger           % arranger
	  }
  }
}

\score {
  \context ChoirStaff <<
    \context Staff = upper <<
      \context Voice =
         sopranos { \voiceOne << \melody >> }
      \context Voice =
         altos { \voiceTwo << \alto >> }
      \context Lyrics = one \lyricsto sopranos \verseOne
      \context Lyrics = two \lyricsto sopranos \verseTwo
    >>
    \context Staff = lower <<
      \clef bass
      \context Voice =
        tenors { \voiceOne << \tenor >> }
      \context Voice =
        basses { \voiceTwo << \bass >> }
    >>
>>

  \layout {
    \context {
      \Score
      % **** Turns off bar numbering
      \remove "Bar_number_engraver"
    }
    \context {
      \Lyrics
      % **** Prevents lyrics from running too close together
      \override LyricSpace #'minimum-distance = #0.6
      % **** Makes the text of lyrics a little smaller
      \override LyricText #'font-size = #-1
      % **** Moves lines of lyrics closer together
      \override VerticalAxisGroup #'minimum-Y-extent = #'(-1 . 1)
    }
  }
}

\markup { 
  \small {
    \fill-line {
      \line {
	"3. "
	\column {
	  "Glory be to God the Father;"
	  "Glory be to God the Son;"
	  "Glory be to God the Spirit;"
	  "Glory to the Three in One."
	  "From the heart of blesséd Mary,"
	  "From all saints the song ascends,"
	  "And the Church the strain re-echoes"
	  "Unto earth's remotest ends."
	}
      }
    }
  }
}

\markup {
  \fill-line {
    " "
    \column {
      \small \caps \poet % poet
    }
  }
}

%{
  Per the _Hymnal 1940 _, 117.
  Change log:
  3-24-06 Moved to 2.8, current formatting
%}


