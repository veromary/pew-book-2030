% Version: 1.10
% Last edit: March 23, 2006

\version "2.8.0"
\include "english.ly"

#(set-default-paper-size "letter")
#(set-global-staff-size 20)

\paper {
	ragged-bottom = ##t 
	top-margin = 0.25\in
	bottom-margin = 0.25\in
}

title = "The Church's One Foundation"
piece = "Aurelia"
composer = "Samuel S. Wesley"
meter = "76.76.D."
arranger = " "
poet = "Samuel John Stone"
translator = " "

musiccopyright = " "
textcopyright = " "



world = {
  \key ef \major
  \time 4/4
  \partial 4*1
}

melody = \relative c'' {
  \world
  g4 |
  g g af g |
  g2 f4 ef |
  ef c' bf af|
  g2. af4 |
  bf ef ef d |
  d2 c4 bf |
  af bf g ef |
  f2. f4 | 
  g af bf c | 
  c2 bf4 ef |
  ef4. d8 c4 g |
  af2. f4 |
  g g af g |
  g2 f4 ef | 
  ef g ef d |
  ef2. \bar "|."
}


alto = \relative c' {
  \world
  ef4 |
  ef ef ef ef | 
  ef2 d4 ef |
  c ef ef d |
  ef2. d4 |
  ef ef ef f |
  f2 ef4 g |
  g f ef ef |
  d2. d4 |
  ef d ef ef |
  ef2 ef4 g |
  g4. g8 g4 g4 |
  f2. f4 |
  ef ef ef ef |
  c2 c4 c |
  c c bf bf |
  bf2.
}

tenor = \relative c' {
  \world
  bf4 | 
  bf bf c bf |
  bf2 bf4 bf |
  f f g bf |
  bf2. bf4 |
  bf bf b b |
  b2 c4 d |
  ef bf bf a |
  bf2. bf4 |
  bf bf bf af |
  af2 bf4 c |
  c4. b8 c4 c |
  c2. bf4 |
  bf bf c bf |
  bf2 af4 af |
  af af f af |
  g2.
}

bass = \relative c {
  \world
  ef4 |
  ef ef ef ef |
  bf2 af4 g |
  af af bf bf |
  ef2. f4 |
  g g g g, |
  af2 af4 bf |
  c d ef c |
  bf2. bf4 |
  ef f g af |
  af2 g4 c, |
  g'4. f8 ef4 e |
  f2. d4 |
  ef ef ef ef |
  af,2 af4 af |
  f f bf bf |
  ef2.
}

verseOne = \lyricmode {
	\set stanza = "1. "
	The Chur -- ch's one foun -- da -- tion
	Is Je -- sus Christ her Lord;
	She is his new cre -- a -- tion
	By wa -- ter and the word;
	From heavn'n he came and sought her
	To be his ho -- ly bride;
	With his own blood he bought her,
	And for her life he died.
}

verseTwo = \lyricmode {
	\set stanza = "2. "
	E -- lect from ev -- 'ry na -- tion,
	Yet one e'er all the earth,
	Her char -- ter of sal -- va -- tion,
	One Lord, one faith, one birth;
	One ho -- ly Name she bless -- es,
	Par -- takes one ho -- ly food,
	And to one hope she press -- es,
	With ev -- 'ry grace en -- dued.
}

verseThree = \lyricmode {
	\set stanza = "3. "
	Though with a scorn -- ful won -- der
	Men see her sore op -- pressed,
	By schi -- sms rent a -- sun -- der,
	By her -- e -- sies dis -- trest;
	Yet saints their watch are keep -- ing,
	Their cry goes up, “How long?” 
	And soon the night of weep -- ing will be the morn of song.
}

\markup {
  \column {
    \fill-line { \large \bold \title } % title
    \fill-line { \caps \piece               % piece
	         \caps \composer      % composer
	  }
    \fill-line { \meter          % meter
	         \arranger           % arranger
	  }
%    \fill-line { " " \musiccopyright }
  }
}

\score {
  \context ChoirStaff <<
    \context Staff = upper <<
      \context Voice =
         sopranos { \voiceOne << \melody >> }
      \context Voice =
         altos { \voiceTwo << \alto >> }
	 \context Lyrics = one \lyricsto sopranos \verseOne
	 \context Lyrics = two \lyricsto sopranos \verseTwo
	 \context Lyrics = three \lyricsto sopranos \verseThree
    >>
    \context Staff = men <<
      \clef bass
      \context Voice =
        tenors { \voiceOne << \tenor >> }
      \context Voice =
        basses { \voiceTwo << \bass >> }
    >>
  >>
  \layout {
    \context {
      \Score
      % **** Turns off bar numbering
      \remove "Bar_number_engraver"
    }
    \context {
      \Lyrics
      % **** Prevents lyrics from running too close together
      \override LyricSpace #'minimum-distance = #0.6
      % **** Makes the text of lyrics a little smaller
      \override LyricText #'font-size = #-1
      % **** Moves lines of lyrics closer together
      \override VerticalAxisGroup #'minimum-Y-extent = #'(-1 . 1)
    }
  }
}

\markup { 
  \small {
    \fill-line {
      \line {
	"4. "
	\column {
	  "'Mid toil and tribulation, And tumult of her war,"
	  "She wait the consummation Of peace for evermore;"
	  "Till with the vision glorious Her longing eyes are blest,"
	  "And the great Church victorious Shall be the Church at rest."
	}
      }
      \line {
	"5. "
	\column {
	  "Yet she on earth hath union With God, the Three in One,"
	  "And mystic sweet communion With those whose rest is won."
	  "O happy ones and holy! Lord, give us grace that we"
	  "Like them, the meek and lowly, On high may dwell with thee."
	}
      }
    }
  }
}

\markup {
  \column {
    \fill-line { " " \small \caps \poet  } % poet
%    \fill-line { " " \small \caps \translator }  % translator
%    \fill-line { " " \small \textcopyright }
  }
}

%{
  Per the _Hymnal 1940 _, 396
  Changelog:
  3/23/06 -- Convert to 2.8, reformat markup verses to outdent verse number,
             change titling
%}


