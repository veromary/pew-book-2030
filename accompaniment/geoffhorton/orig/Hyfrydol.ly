% Version 1.0
% Last edit: March 24, 2006
% The music and words produced by this source code are believed
% to be in the public domain in the United States. The source
% code itself is covered by the Creative Commons Attribution-
% NonCommercial license, 
% http://creativecommons.org/licenses/by-nc/2.5/
% Attribution: Geoff Horton

\version "2.8.0"
\include "english.ly"

#(set-default-paper-size "letter")
#(set-global-staff-size 20)

title = "Love Divine, All Loves Excelling"
composer = "Rowland Hugh Prichard"
poet = "Charles Wesley, alt."
meter = "87.87.D"
piece = "Hyfrydol"
arranger = ""

world = {
  \key f \major
  \time 3/4
}

melody = \relative c' {
  \world
  \autoBeamOff
  f2 g4 |
  f4.( g8) a4 |
  bf2 a4 |
  g( f) g |
  c2 bf4 |
  a2 a4 |
  g( f) g |
  f2. | % line 1
  f2 g4 |
  f4.( g8) a4 |
  bf2 a4 |
  g( f) g |
  c2 bf4 |
  a2 a4 |
  g( f) g |
  f2. | % line 2
  c'2 c4 |
  c4( bf) a |
  bf2 bf4 |
  bf4( a) g |
  a2 a4 |
  a( bf) c |
  c( bf) a |
  g2. | % line 3
  c4( a) c |
  bf( g) bf |
  a( f) a |
  g8[ a bf a] g4 |
  c2 c4 |
  d( c) bf |
  a2 g4 |
  f2. \bar "|."
}


alto = \relative c' {
  \world
  \autoBeamOff
  c2 e4 |
  f2 f4 |
  d4( g) f |
  e( d) e |
  f( c) e8[ f] |
  f4( e) d |
  d2 e4 |
  f2. | % line 1
  c2 e4 |
  f2 f4 |
  d( g) f |
  e( d) e |
  f( c) d8[ e] |
  f2 e4 |
  d2 e4 |
  f2. | % line 2
  e2 e4 |
  f2 f4 |
  f( e) d |
  e2 e4 |
  f2 f4 |
  f( g) a |
  a( g) f |
  f2( e4) | % line 3
  f2 e4 |
  g2 g4 |
  f( c) c |
  c2 c4 |
  c( f) e |
  d( f) f |
  f2 e4 |
  f2. 
}

tenor = \relative c' {
  \world
  \autoBeamOff
  a2 g4 |
  a4.( bf8) c4 |
  g( c) c |
  c( a) c |
  c( a) bf |
  c2 a4 |
  bf( a) c |
  a2. | % line 1
  a2 g4 |
  a4.( bf8) c4 |
  g( c) c |
  c( a) c |
  c( a) bf |
  c2 c4 |
  bf( a) c |
  a2. | % line 2
  a2 a4 |
  f( g) a |
  g2 g4 |
  g( a) bf |
  a( c) c |
  d2 c4 |
  d2 d4 |
  g,2. | % line 3
  a4( c) c |
  d2 c4 |
  c( a) f |
  e8[ f g f] e4 |
  f( a) c |
  bf( c) d |
  c2 bf4 |
  a2.
}

bass = \relative c {
  \world
  f2 c4 |
  f2 f4 |
  f( e) f |
  c( d) c |
  a( a') g |
  f2 f4 |
  d2 c4 |
  f2. | % line 1
  f,4( a) c |
  f2 f4 |
  f( e) f |
  c( d) c |
  a( a') g |
  f2 a,4 |
  bf( d) c |
  f2. | % line 2
  a2 a4 |
  d,2 d4 |
  g2 g4 |
  c,2 c4 |
  f2 e4 |
  d( bf) a |
  g2 bf4 |
  c2. | % line 3
  a'4( f) a |
  g4.( f8) e4 |
  f2 a,4 |
  c2 c8[ bf] |
  a2 a4 |
  bf( a) bf |
  c2 c4 |
  f2.
}

verseOne = \lyricmode {
  \set stanza = "1. "
  Love di -- vine, all loves ex -- cel -- ling,
  Joy of heav'n, to earth come down,
  Fix in us thy hum -- ble dwell -- ing,
  All thy faith -- ful mer -- cies crown.
  Je -- sus, thou art all com -- pas -- sion,
  Pure, un -- bound -- ed love thou art;
  Vis -- it us with thy sal -- va -- tion,
  En -- ter ev -- 'ry trem -- bling heart.
}

verseTwo = \lyricmode {
  \set stanza = "2. "
  Come, al -- might -- y to de -- liv -- er,
  Let us all thy life re -- ceive;
  Sud -- den -- ly re -- turn, and nev -- er,
  Nev -- er -- more thy temp -- les leave.
  Thee we would be al -- way bless -- ing,
  Serve thee as thy hosts a -- bove,
  Pray, and praise thee with -- out ceas -- ing,
  Glo -- ry in thy per -- fect love.
}

verseThree = \lyricmode {
  \set stanza = "3. "
  Fin -- ish then thy new cre -- a -- tion;
  Pure and spot -- less let us be:
  Let us see thy great sal -- va -- tion
  Per -- fect -- ly re -- stored in thee:
  Changed from glo -- ry in -- to glo -- ry,
  Till in heav'n we take our place,
  Till we cast our crowns be -- fore thee,
  Lost in won -- der, love, and praise.
}

\markup {
  \column {
    \fill-line { \large \bold \title } % title
    \fill-line { \caps \piece               % piece
	         \caps \composer      % composer
	  }
    \fill-line { \meter          % meter
		 \arranger       % arranger
	  }
  }
}

\score {
  \context ChoirStaff <<
    \context Staff = upper <<
      \context Voice =
         sopranos { \voiceOne << \melody >> }
      \context Voice =
         altos { \voiceTwo << \alto >> }
      \context Lyrics = one \lyricsto sopranos \verseOne
      \context Lyrics = two \lyricsto sopranos \verseTwo
      \context Lyrics = three \lyricsto sopranos \verseThree
    >>
    \context Staff = lower <<
      \clef bass
      \context Voice =
        tenors { \voiceOne << \tenor >> }
      \context Voice =
        basses { \voiceTwo << \bass >> }
    >>
  >>
  \layout {
    \context {
      \Score
      % **** Turns off bar numbering
      \remove "Bar_number_engraver"
    }
    \context {
      \Lyrics
      % **** Prevents lyrics from running too close together
      \override LyricSpace #'minimum-distance = #0.6
      % **** Makes the text of lyrics a little smaller
      \override LyricText #'font-size = #-1
      % **** Moves lines of lyrics closer together
      \override VerticalAxisGroup #'minimum-Y-extent = #'(-1 . 1)
    }
  }
}

\markup {
  \fill-line {
    " "
    \column {
      \small \caps \poet % poet
    }
  }
}

\paper {
  ragged-bottom = ##t
  top-margin = 0.25\in
  bottom-margin = 0.25\in
}

%{
  Per _The Hymnal 1940_, #479, first tune
  Change log:
  3-24-06 New hymn
%}


