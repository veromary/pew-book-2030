% Version 1.0
% Last edit: March 21, 2006

% **** This is a template for standard 4-part hymns. It contains most
% **** common options. Comment out the ones you don't need.

\version "2.7.38"
\include "english.ly"

#(set-default-paper-size "letter")
#(set-global-staff-size 20)

\header {
  subtitle = "All Praise to Thee, My God, This Night"
  composer = "Thomas Tallis"
  poet = "Thomas Ken"
  piece = "Tallis' Canon"
  meter = "L.M."
  arranger = "as a canon"
}

world = {
  \key g \major
  \time 4/2
  \partial 4*2
}

melody = \relative c'' {
  \world
  g2 | 
  g fs g g |
  a a b g |
  c c b b |
  a a g d' |
  c a b b |
  a a g d |
  e fs g b |
  a a g1 \bar "|."
}


alto = \relative c' {
  \world
  d2 |
  d d b d |
  e d d g |
  e fs g d |
  e d d g |
  e d d d |
  e fs g g, |
  e' d b d |
  c a b1 |
}

tenor = \relative c' {
  \world
  b2 |
  a a g g |
  g fs g g |
  a a b g |
  c c b b |
  a a g d' |
  c a b b |
  a a g d |
  e fs g1 \bar "|."
}

bass = \relative c' {
  \world
  g2 |
  d d e b |
  c d g, b |
  a a g g' |
  g fs g g |
  a fs g g, |
  c d g, b |
  c d e b |
  c d g,1 |
}

verseOne = \lyricmode {
  \set stanza = "1. "
  All praise to thee, my God, this night
  For all the bless -- ings of the light:
  Keep me, O keep me, King of Kings,
  Be -- neath thine own al -- might -- y wings.
}

verseTwo = \lyricmode {
  \set stanza = "2. "
  For -- give me, Lord, for thy dear Son,
  The ill that I this day have done;
  That with the world, my -- self, and thee,
  I, ere I sleep, at peace may be.
}

verseThree = \lyricmode {
  \set stanza = "3. "
  O may my soul on thee re -- pose,
  And with sweet sleep mine eye -- lids close;
  Sleep that shall me more vig -- 'rous make
  To serve my God when I a -- wake.
}

\score {
  \context ChoirStaff <<
    \context Staff = upper <<
      \context Voice =
         sopranos { \voiceOne << \melody >> }
      \context Voice =
         altos { \voiceTwo << \alto >> }
      \context Lyrics = one \lyricsto sopranos \verseOne
      \context Lyrics = two \lyricsto sopranos \verseTwo
      \context Lyrics = three \lyricsto sopranos \verseThree
    >>
    \context Staff = lower <<
      \clef bass
      \context Voice =
        tenors { \voiceOne << \tenor >> }
      \context Voice =
        basses { \voiceTwo << \bass >> }
    >>
>>
  \layout {
    \context {
      \Score
      % **** Turns off bar numbering
      \remove "Bar_number_engraver"
    }
    \context {
      \Lyrics
      % **** Prevents lyrics from running too close together
      \override LyricSpace #'minimum-distance = #0.6
      % **** Makes the text of lyrics a little smaller
      \override LyricText #'font-size = #-1
      % **** Moves lines of lyrics closer together
      \override VerticalAxisGroup #'minimum-Y-extent = #'(-1.5 . 1.5)
    }
  }
}


\markup { 
  \normalsize {
    \fill-line {
      \hspace #3.0
      \column {
	"4. Praise God, from whom all blessings flow;"
	"Praise him, all creatures here below;"
	"Praise him above, ye heav'nly host:"
	"Praise Father, Son, and Holy Ghost."
      }
      \hspace #3.0
    }
  }
}

\paper {
  ragged-bottom = ##t
  top-margin = 0.25\in
  bottom-margin = 0.25\in
}

%{
  _The Hymnal, 1940_, #165. Tallis' own harmonization is different
  and can be found in TallisCanon2.ly.
%}


