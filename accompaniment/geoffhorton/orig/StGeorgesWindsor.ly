% Version 1.20
% Last edit: March 25, 2006
% The music and words produced by this source code are believed
% to be in the public domain in the United States. The source
% code itself is covered by the Creative Commons Attribution-
% NonCommercial license, 
% http://creativecommons.org/licenses/by-nc/2.5/
% Attribution: Geoff Horton

\version "2.8.0"
\include "english.ly"

#(set-default-paper-size "letter")
#(set-global-staff-size 18)
#(ly:set-option (quote no-point-and-click))

title = "Come, Ye Thankful People, Come"
composer = "George J. Elvey"
poet = "Henry Alford, alt."
piece = "St. George's Windsor"
meter = "77.77.77.77"
arranger = ""

world = {
  \key f \major
  \time 4/4
}

melody = \relative c'' {
  \world
  a4. a8 c4 a |
  f g a2 |
  a4. a8 c4 a |
  f g a2 |
  a4. a8 bf4 bf |
  g4. g8 a2 |
  a4 b c f, |
  e d c2 |
  e4. e8 g4 e |
  f g a2 |
  a4. a8 c4 a |
  bf c d2 |
  d4. d8 bf4 g |
  c4. c8 a2 |
  bf4 d c f, |
  a g f2 \bar "|."
}


alto = \relative c' {
  \world
  c4. c8 c4 c |
  d e f2 |
  c4. c8 c4 c |
  d d cs2 |
  d4. d8 d4 d |
  c c bf2 |
  c4 d c d |
  c b c2 |
  c4. c8 c4 c |
  c bf a2 |
  c4. c8 f4 f |
  f ef d2 |
  d4 fs g d |
  c e f2 |
  f4 f f f |
  f e f2 |
}

tenor = \relative c {
  \world
  f4. f8 g4 a |
  a c c2 |
  f,4. f8 g4 f |
  a bf e,2 |
  f4. f8 g4 g |
  e e f2 |
  f4 f g a |
  g4. f8 e2 |
  g4. g8 e4 g |
  f4. e8 f2 |
  f4. f8 a4 c |
  bf4. a8 bf2 |
  a4 d d bf |
  g c c2 |
  bf4 bf c a |
  c4. bf8 a2 |
}

bass = \relative c {
  \world
  f4. f8 e4 f |
  d c f2 |
  f4. f8 e4 f |
  d bf a2 |
  d4. d8 g,4 g |
  c c f,2 |
  f'4 d e f |
  g g, c2 |
  c4. c8 c4 bf |
  a g f2 |
  f'4. f8 f4 ef |
  d c bf2 |
  fs'4 d g g |
  e c f2 |
  d4 bf a d |
  c c f2 |
}

verseOne = \lyricmode {
  \set stanza = "1. "
  Come, ye thank -- ful peo -- ple, come,
  Raise the song of har -- vest home;
  All is safe -- ly gath -- ered in,
  Ere the win -- ter storms be -- gin;
  God, our Ma -- ker, doth pro -- vide
  For our wants to be sup -- plied;
  Come to God's own tem -- ple, come,
  Raise the song of har -- vest -- home.
}

verseTwo = \lyricmode {
  \set stanza = "2. "
  All the world is God's own field,
  Fruit un -- to his praise to yield;
  Wheat and tares to -- geth -- er sown,
  Un -- to joy or sor -- row grown;
  First the blade, and then the ear,
  Then the full corn shall ap -- pear:
  Grant, O har -- vest Lord, that we
  Whole -- some grain and pure may be.
}

\markup {
  \column {
    \fill-line { \large \bold \title } % title
    \fill-line { \caps \piece               % piece
	         \caps \composer      % composer
	  }
    \fill-line { \meter          % meter
	         \arranger           % arranger
	  }
  }
}

\score {
  \context ChoirStaff <<
    \context Staff = upper <<
      \context Voice =
         sopranos { \voiceOne << \melody >> }
      \context Voice =
         altos { \voiceTwo << \alto >> }
      \context Lyrics = one \lyricsto sopranos \verseOne
      \context Lyrics = two \lyricsto sopranos \verseTwo
    >>
    \context Staff = lower <<
      \clef bass
      \context Voice =
        tenors { \voiceOne << \tenor >> }
      \context Voice =
        basses { \voiceTwo << \bass >> }
    >>
  >>
  \layout {
    \context {
      \Staff
      \override NoteCollision #'merge-differently-dotted = ##t
    }
    \context {
      \Score
      % **** Turns off bar numbering
      \remove "Bar_number_engraver"
    }
    \context {
      \Lyrics
      % **** Prevents lyrics from running too close together
      \override LyricSpace #'minimum-distance = #0.6
      % **** Makes the text of lyrics a little smaller
      \override LyricText #'font-size = #-1
      % **** Moves lines of lyrics closer together
      \override VerticalAxisGroup #'minimum-Y-extent = #'(-1 . 1)
    }
  }
}

\markup { 
  \small {
    \fill-line {
      \hspace #3.0
      \line {
	"3. "
	\column {
	  "For the Lord our God shall come,"
	  "And shall take his harvest home;"
	  "From his field shall in that day"
	  "All offences purge away;"
	  "Give his angels charge at last"
	  "In the fire the tares to cast"
	  "But the fruitful ears to store"
	  "In his garner evermore."
	}
      }
      \hspace #3.0
      \line {
	"4. "
	\column {
	  "Even so, Lord, quickly come"
	  "To thy final harvest-home;"
	  "Gather thou thy people in,"
	  "Free from sorrow, free from sin;"
	  "There, for ever purified,"
	  "In thy presence to abide:"
	  "Come, with all thy angels, come,"
	  "Raise the glorious harvest-home."
	}
      }
      \hspace #3.0
    }
  }
}

\markup {
  \fill-line {
    " "
    \column {
      \small \caps \poet % poet
    }
  }
}

\paper {
  ragged-bottom = ##t
  top-margin = 0.25\in
  bottom-margin = 0.25\in
}

%{
  Per the _Hymnal 1940 _, #137
  Change log:
  3-25-06 move to 2.8, new formatting
%}


