% Version 1.0
% Last edit: March 30, 2006
% The music and words produced by this source code are believed
% to be in the public domain in the United States. The source
% code itself is covered by the Creative Commons Attribution-
% NonCommercial license, 
% http://creativecommons.org/licenses/by-nc/2.5/
% Attribution: Geoff Horton

\version "2.8.0"
\include "english.ly"

#(set-default-paper-size "letter")
#(set-global-staff-size 20)

title = "O Sacred Head Surrounded"
composer = "Hans Leo Hassler"
poet = "ascr. St. Bernard of Clairvaux"
translator = "trans. Robert Bridges"
meter = "76.76.D"
arranger = "Arr. J.S. Bach"
piece = "Passion Chorale"

world = {
  \key a \minor
  \time 4/2
  \partial 2*1
}

melody = \relative c' {
  \world
  e2 |
  a g f e |
  d1 e2 b' |
  c c b4( a) b2 |
  a1. % end line 1
  e2 |
  a g f e |
  d1 e2 b' |
  c c b4( a) b2 |
  a1. % end line 2
  c2 |
  b4( a) g2 a b |
  c1 c2 g |
  a g f f |
  e1. % end line 3
  c'!2 |
  b4( c) d2 c b |
  a1 b2 e, |
  f e d g |
  e1. \bar "|."
}


alto = \relative c' {
  \world
  c2 |
  c c c4( d) d( c) |
  c2( b) c d |
  c4( d) e2 e2. d4 |
  c1. % end line 1
  c2 |
  c c c4( d) d( c) |
  c2( b) c d |
  c4( d) e2 e2. d4 |
  c1. % end line 2
  e2 |
  d4( f) e( d) c2 f |
  f2( e4 d) e2 e |
  f e e d |
  cs1. % end line 3
  d2 |
  d d e4( fs) g2 |
  g( fs) g c,4( b) |
  a( b) c2 c4( a) b2 |
  c1. \bar "|."
}

tenor = \relative c' {
  \world
  g2 |
  f2 g a4( g) g2 |
  a( g) g gs |
  a a a gs |
  a1. % end line 1
  g2 |
  f2 g a4( g) g2 |
  a( g) g gs |
  a a a gs |
  a1. % end line 2
  a4( g) |
  f( a) c( b) a( g) f( g) |
  a2( g4 f) g2 c |
  c bf a a |
  a1. % end line 3
  a=2 |
  g a g4( a) b( a) |
  e'2( d) d g, |
  f g g g |
  g1. \bar "|."
}

bass = \relative c {
  \world
  c2 |
  f e a,4( b) c2 |
  f,( g) c b |
  a4( b) c( d) e2 e, |
  a1. % end line 1
  c2 |
  f e a,4( b) c2 |
  f,( g) c b |
  a4( b) c( d) e2 e, |
  a1. % end line 2
  a2 |
  d e f4( e) d2 |
  c1 c2 c |
  f4( e) d( cs) d( e) f( g) |
  a1. % end line 3
  fs2 |
  g fs e d |
  cs2( d) g, c= |
  d e4( f) g2 g, |
  c1. \bar "|."
  
}

verseOne = \lyricmode {
  \set stanza = "1. "
  O sa -- cred head, sore wound -- ed,
  De -- filed and put to scorn,
  O king -- ly head, sur -- round -- ed 
  With mock -- ing crown of thorn;
  What sor -- row mars thy grand -- eur? 
  Can death thy bloom de -- flow'r?
  O coun -- te -- nance whose splen -- dor
  The hosts of heav'n a -- dore!
}

verseTwo = \lyricmode {
  \set stanza = "2. "
  Thy beau -- ty, long de -- sir --ed,
  Hath van -- ished from our sight;
  Thy pow'r is all ex -- pir -- ed,
  And quench'd the light of light;
  Ah me! for whom thou di -- est,
  Hide not so far thy grace:
  Show me, O Love most high -- est,
  The bright -- ness of thy face.
}

\markup {
  \column {
    \fill-line { \large \bold \title } % title
    \fill-line { \caps \piece               % piece
	         \caps \composer      % composer
	  }
    \fill-line { \meter          % meter
	         \arranger           % arranger
	  }
  }
}

\score {
  \context ChoirStaff <<
    \context Staff = upper <<
      \context Voice =
         sopranos { \voiceOne << \melody >> }
      \context Voice =
         altos { \voiceTwo << \alto >> }
      \context Lyrics = one \lyricsto sopranos \verseOne
      \context Lyrics = two \lyricsto sopranos \verseTwo
    >>
    \context Staff = lower <<
      \clef bass
      \context Voice =
        tenors { \voiceOne << \tenor >> }
      \context Voice =
        basses { \voiceTwo << \bass >> }
    >>
>>
  \layout {
    \context {
      \Score
      % **** Turns off bar numbering
      \remove "Bar_number_engraver"
    }
    \context {
      \Lyrics
      % **** Prevents lyrics from running too close together
      \override LyricSpace #'minimum-distance = #0.6
      % **** Makes the text of lyrics a little smaller
      \override LyricText #'font-size = #-1
      % **** Moves lines of lyrics closer together
      \override VerticalAxisGroup #'minimum-Y-extent = #'(-1 . 1)
    }
  }
}


\markup { 
  \normalsize {
    \fill-line {
      \hspace #3.0
      \line {
	"3. "
	\column {
	  "In thy most bitter passion"
	  "  My heart to share doth cry,"
	  "With thee for my salvation"
	  "  Upon the cross to die."
	  "Ah, keep my heart this movéd"
	  "  To stand thy cross beneath,"
	  "To mourn thee, well-belovéd,"
	  "  Yet thank thee for thy death."
	}
      }
      \hspace #3.0
      \line {
	"4. "
	\column {
	  "My days are few, O fail not,"
	  "  With thine immortal pow'r,"
	  "To hold me that I quail not"
	  "  In death's most fearful hour:"
	  "That I may fight befriended"
	  "  And see in my last strife"
	  "To me thine arms extended"
	  "  Upon the cross of life."
	}
      }
      \hspace #3.0
    }
  }
}

\markup {
  \fill-line {
    " "
    \column {
      \small \caps \poet % poet
      \small \caps \translator % translator
    }
  }
}



\paper {
  ragged-bottom = ##t
  top-margin = 0.0\in
  bottom-margin = 0.25\in
}

%{
  _The Hymnal 1940_, #75. Note that the _Hymnal_ itself is wrong
  about the author, as _The Hymnal Companion_ states that Bridge's
  translation is from the Latin original and not from Paulus Gerhardt's
  German translation.

  Revision history: 
  3-30-06 New hymn
%}


