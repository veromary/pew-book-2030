% Version 1.0
% Last edit: March 26, 2006
% The music and words produced by this source code are believed
% to be in the public domain in the United States. The source
% code itself is covered by the Creative Commons Attribution-
% NonCommercial license, 
% http://creativecommons.org/licenses/by-nc/2.5/
% Attribution: Geoff Horton

\version "2.8.0"
\include "english.ly"

#(set-default-paper-size "letter")
#(set-global-staff-size 20)

title = "Jesus Christ is Ris'n Today"
composer = \markup \italic { "Lyra Davidica" }
poet = "Latin, 14th cent."
translator = "Trans. Tate and Brady; St. 4, Charles Wesley"
meter = "77.77, with Alleluias"
arranger = "Altered, 1749"
piece = "Easter Hymn"

world = {
  \key c \major
  \time 4/4
}

melody = \relative c' {
  \world
  \autoBeamOff
  c4 e g c, |
  f a a( g) |
  e8[( f g c,] f4) e8[ f] |
  e4( d) c2 | % end line 1
  f4 g a g |
  f e e( d) |
  e8[( f g c,] f4) e8[ f] |
  e4( d) c2 | % end line 2
  b'4 c d g, |
  c d e2 |
  b8[( c d g,] c4) b8[ c] |
  b4( a) g2 | % end line 3
  g8[ a] b[ g] c4 c, |
  f a a( g) |
  c8[( b c g] [a b]) c[ d] |
  c4( b) c2 
}


alto = \relative c' {
  \world
  c4 c d c |
  c f f( e) |
  c2( c8[ b]) c4 |
  c( b) c2 | % end line 1
  c4 c c c |
  c8[ b] c4 c( b) |
  c2( c8[ b]) c4 |
  c( b) c2 | % end line 2
  g'4. fs8 g4 g |
  g f e2 |
  g2( ~ g8[ fs]) g4 |
  g( fs) g2 |
  b,8[ c] d[ b] c4 c |
  c f f( e) |
  e8[( f g e] f4) e8[ f] |
  e4( g8[ f]) e2
}

tenor = \relative c' {
  \world

}

bass = \relative c {
  \world

}

verseOne = \lyricmode {
  \set stanza = "1. "

}

\markup {
  \column {
    \fill-line { \large \bold \title } % title
    \fill-line { \caps \piece               % piece
	         \caps \composer      % composer
	  }
    \fill-line { \meter          % meter
	         \arranger           % arranger
	  }
  }
}

\score {
  \context ChoirStaff <<
    \context Staff = upper <<
      \context Voice =
         sopranos { \voiceOne << \melody >> }
      \context Voice =
         altos { \voiceTwo << \alto >> }
      \context Lyrics = one \lyricsto sopranos \verseOne
    >>
    \context Staff = lower <<
      \clef bass
      \context Voice =
        tenors { \voiceOne << \tenor >> }
      \context Voice =
        basses { \voiceTwo << \bass >> }
    >>
>>
  \layout {
    \context {
      \Score
      % **** Turns off bar numbering
      \remove "Bar_number_engraver"
    }
    \context {
      \Lyrics
      % **** Prevents lyrics from running too close together
      \override LyricSpace #'minimum-distance = #0.6
      % **** Makes the text of lyrics a little smaller
      \override LyricText #'font-size = #-1
      % **** Moves lines of lyrics closer together
      \override VerticalAxisGroup #'minimum-Y-extent = #'(-1 . 1)
    }
  }
}


\markup { 
  \normalsize {
    \fill-line {
      \column {

      }
      \hspace #3.0
      \column {

      }
    }
  }
}

\markup {
  \fill-line {
    " "
    \column {
      \small \caps \poet % poet
    }
  }
}



\paper {
  ragged-bottom = ##t
  top-margin = 0.25\in
  bottom-margin = 0.25\in
}

%{
  Source info goes here.
  “  ”   (real quotation marks for easy cut-and-pasting)
  © for cut-and-pasting.
  Revision history: 
  3-23-06 Added licensing info, revised title layout system
%}


