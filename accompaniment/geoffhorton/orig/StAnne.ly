% Version 1.1
% Last edit: March 24, 2006
% The music and words produced by this source code are believed
% to be in the public domain in the United States. The source
% code itself is covered by the Creative Commons Attribution-
% NonCommercial license, 
% http://creativecommons.org/licenses/by-nc/2.5/
% Attribution: Geoff Horton

\version "2.8.0"
\include "english.ly"

#(set-default-paper-size "letter")
#(set-global-staff-size 20)

title = "O God, Our Help in Ages Past"
composer = "William Croft"
poet = "Isaac Watts, based on Psalm 90"
piece = "St. Anne"
meter = "C.M."
arranger = ""

world = {
  \key bf \major
  \time 4/2
  \partial 2*1
}

melody = \relative c' {
  \world
  f2 |
  d g f bf |
  bf a bf f |
  bf f g e |
  f1. \bar "||" \break a2 |
  bf2 g c a |
  bf g a f |
  g bf c a |
  bf1. \bar "|."        
}


alto = \relative c' {
  \world
  bf2 |
  bf bf bf d |
  g f f d |
  d d d c |
  c1. c2 |
  d bf ef c |
  bf g' fs f |
  ef d c c |
  d1.
}

tenor = \relative c {
  \world
  d2 |
  f g bf bf |
  c c d bf |
  bf a g g |
  a1. f2 |
  f g g a |
  d2. c4 a2 bf |
  ef, f g f |
  f1.
}

bass = \relative c {
  \world
  bf2 |
  bf ef d g |
  ef f bf, bf |
  g d' bf c | 
  < \tweak #'font-size #-2 f f,>1. f2 |
  bf, ef c f | 
  g ef d d |
  c bf ef f |
  bf,1.
}

verseOne = \lyricmode {
  \set stanza = "1. "
  O God, our help in a -- ges past,
  Our hope for years to come,
  Our shel -- ter from the storm -- y blast,
  And our e -- ter -- nal home;
}

verseTwo = \lyricmode {
  \set stanza = "2. "
  Un -- der the sha -- dow of thy throne,
  Thy saints have dwelt se -- cure;
  Suf -- fi -- cient is thine arm a -- lone,
  And our de -- fence is sure.
}

verseThree = \lyricmode {
  \set stanza = "3. "
  Be -- fore the hills in or -- der stood,
  Or earth re -- ceived her frame,
  From ev -- er -- last -- ing thou art God,
  To end -- less years the same.
}

\markup {
  \column {
    \fill-line { \large \bold \title } % title
    \fill-line { \caps \piece               % piece
	         \caps \composer      % composer
	  }
    \fill-line { \meter          % meter
	         \arranger           % arranger
	  }
  }
}

\score {
  \context ChoirStaff <<
    \context Staff = upper <<
      \context Voice =
         sopranos { \voiceOne << \melody >> }
      \context Voice =
         altos { \voiceTwo << \alto >> }
      \context Lyrics = one \lyricsto sopranos \verseOne
      \context Lyrics = two \lyricsto sopranos \verseTwo
    >>
    \context Staff = lower <<
      \clef bass
      \context Voice =
        tenors { \voiceOne << \tenor >> }
      \context Voice =
        basses { \voiceTwo << \bass >> }
    >>
  >>
  \layout {
    \context {
      \Score
      % **** Turns off bar numbering
      \remove "Bar_number_engraver"
    }
    \context {
      \Lyrics
      % **** Prevents lyrics from running too close together
      \override LyricSpace #'minimum-distance = #0.6
      % **** Makes the text of lyrics a little smaller
      \override LyricText #'font-size = #-1
      % **** Moves lines of lyrics closer together
      \override VerticalAxisGroup #'minimum-Y-extent = #'(-1 . 1)
    }
  }
}


\markup { 
  \normalsize {
    \fill-line {
      \hspace #3.0
      \column {
	\line {
	  "4. "
	  \column {
	    "A thousand ages in thy sight"
	    "  Are like an evening gone;"
	    "Short as the watch that ends the night"
	    "  Before the rising sun."
	  }
	}
	\hspace #1
	\line {
	  "5. "
	  \column {
	    "Time, like an ever-rolling stream,"
	    "  Bears all its sons away;"
	    "They fly, forgotten, as a dream"
	    "  Dies at the op'ning day."
	  }
	}
	\hspace #1
	\line {
	  "6. "
	  \column {
	    "O God, our help in ages past,"
	    "  Our hope for years to come,"
	    "Be thou our guide while life shall last,"
	    "  And our eternal home."
	  }
	}
      }
      \hspace #3.0
    }
  }
}


\markup {
  \fill-line {
    " "
    \column {
      \small \caps \poet % poet
    }
  }
}

\paper {
  ragged-bottom = ##t
  top-margin = 0.25\in
  bottom-margin = 0.25\in
}

%{
  Per _The Hymnal 1940_, #289
  Change log:
  3-24-06 Added all verses, moved to 2.8 and current formatting
%}


