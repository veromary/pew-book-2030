% Version 1.2
% Last edit: March 10, 2006
% The music and words produced by this source code are believed
% to be in the public domain in the United States. The source
% code itself is covered by the Creative Commons Attribution-
% NonCommercial license, 
% http://creativecommons.org/licenses/by-nc/2.5/
% Attribution: Geoff Horton

% **** Note: Running this file through lilypond produces a lot of
% **** "programming error: adding reverse spring, setting to unit
% **** continuing, cross fingers" error messages. It doesn't seem
% **** to affect the final output, so they can safely be ignored.

\version "2.8.0"
\include "english.ly"

#(set-default-paper-size "letter")
#(set-global-staff-size 20)

% *** play with sizes a little to get it to fit on one page

\paper {
  ragged-bottom = ##t
  top-margin = 0.25\in
  bottom-margin = 0.25\in
  between-system-space = 0.25\in
  head-separation = 0\in
  page-top-space = 0\in
}

title = "Lo! he comes with clouds descending"
composer = \markup { \italic "Select Hymns with Tunes Annext" }
poet =  \markup { "Charles Wesley, " \italic " alt" }
meter = "87.87.12 7"
piece = "Helmsley"
arranger = ""

world = {
  \key g \major
  \time 2/2
}

optSlur = \tag #'withoutSlur { \once \override Slur #'transparent = ##t }

melody = \relative c'' {
  \world
  \autoBeamOff
  g2 \optSlur b4( d) |
  \optSlur g,( fs) \optSlur e( d) |
  \optSlur e4.( fs8 g4) fs8[ e] |
  \optSlur d4.( c8) b2 | d2. d4 |
  g2 a | \optSlur b4( d) \optSlur c( b) |
  \optSlur b2( a) |
  g2 \optSlur b4( d) |
  \optSlur g,( fs) \optSlur e( d) |
  \optSlur e4.( fs8 g4) fs8[ e] |
  \optSlur d4.( c8) b2 |
  d2. d4 |
  g2 a |
  \optSlur b4( d) \optSlur c( b) |
  \optSlur b2( a) |
  \optSlur a4.( b8 a4) b |
  c2 b |
  \optSlur g4.( a8 g4) c |
  \optSlur b( a) g2 |
  \optSlur b4.( c8 b4) d |
  \optSlur c( b) a2 |
  g \optSlur a4( b) |
  d,2 c' |
  b a |
  g1 \bar "|."
}


alto = \relative c' {
  \world
  d2 d2 |
  b4 d c d |
  c4. d8 e4 d8 c |
  a2 g |
  a2. d4 |
  d2 e4 fs |
  g b a g |
  g2 fs |
  d2 d2 |
  b4 d c d |
  c4. d8 e4 d8 c |
  a2 g |
  a2. d4 |
  d2 e4 fs |
  g b a g |
  g2 fs |
  fs4. g8 fs4 g |
  e fs g2 |
  d e4 e |
  g fs e2 |
  e2. d4 |
  fs g ~ g fs |
  d2 ~ d ~ |
  d g ~ |
  g fs |
  g1 \bar "|."
}

tenor = \relative c' {
  \world
  b2 b4 a |
  g b g8 a b4 |
  g2. g4 |
  fs2 g |
  fs4 g fs2 |
  g2 c |
  b e |
  d1 |
  b2 b4 a |
  g b g8 a b4 |
  g2. g4 |
  fs2 g |
  fs4 g fs2 |
  g2 c |
  b e |
  d1 |
  d2. d4 |
  c2 d |
  b4. a8 b4 c |
  d4. c8 b2 |
  g4. a8 g4 g |
  a b8 c d2 |
  d c4 b |
  a2 g4 a |
  b c d c |
  b1 \bar "|."
}

bass = \relative c' {
  \world
  g2 g4 fs |
  e b c g |
  c2. c4 |
  d2 g, |
  d'4 e d c |
  b2 a |
  g c |
  d1 |
  g2 g4 fs |
  e b c g |
  c2. c4 |
  d2 g, |
  d'4 e d c |
  b2 a |
  g c |
  d1 |
  d2. b4 |
  a2 g |
  g'4. fs8 e4 a, |
  b8 c d4 e2 |
  e2. b4 |
  a g d'2 |
  b' a4 g |
  fs2 e |
  d d |
  g,1 \bar "|."
}


verseOne = \lyricmode {
  \set stanza = "1. "
  Lo! He comes, with clouds de -- scend -- ing
  Once for our sal -- va -- tion slain;
  Thou -- sand thou -- sand saints at -- tend -- ing
  Swell the tri -- umph of his train;
  Al -- le -- lu -- ia, al -- le -- lu -- ia, al -- le -- lu -- ia!
  Christ the Lord re -- turns to reign.
}

verseTwo = \lyricmode {
  \set stanza = "2. "
  Ev -- 'ry eye shall now be -- hold him,
  Robed in dread -- ful ma -- jes -- ty;
  Those who set at naught and sold him,
  Pierced, and nailed him to the tree,
  Deep -- ly wail -- ing, deep -- ly wail -- ing, deep -- ly wail -- ing,
  Shall the true Mes -- si -- ah see.
}

verseThree = \lyricmode {
  \set stanza = "3. "
  Those dear to -- kens of his pas -- sion
  Still his dazz -- ling bo -- dy bears,
  Cause of end -- less ex -- ul -- ta -- tion
  To his ran -- somed wor -- ship --pers:
  With what rap -- ture, with what rap -- ture, with what rap -- ture
  Gaze we on those glo -- rious scars!
}

verseFour = \lyricmode {
  \set stanza = "4. "
  Yea, A -- men! let all a -- dore thee,
  High on thine e -- ter -- nal throne;
  Sa -- viour, take the pow'r and glo -- ry;
  Claim the king -- dom for thine own:
  Al -- le -- lu -- ia, al -- le -- lu -- ia, al -- le -- lu -- ia!
  Thou shalt reign, and thou a -- lone.
}

\markup {
  \column {
    \fill-line { \large \bold \title } % title
    \fill-line { \caps \piece               % piece
	         \caps \composer      % composer
	  }
    \fill-line { \meter          % meter
	         \arranger           % arranger
	  }
  }
}

\score {
  <<
    \context Staff = melody <<
      \removeWithTag #'withoutSlur
      \context Voice = melody { \melody } 
      \context Lyrics = one \lyricsto melody \verseOne
      \context Lyrics = two \lyricsto melody \verseTwo
      \context Lyrics = three \lyricsto melody \verseThree
      \context Lyrics = four \lyricsto melody \verseFour
    >>
    \context PianoStaff <<
      \context Staff = upper <<
        \set Staff.printPartCombineTexts = ##f
        \partcombine
        \melody
        \alto
      >>
      \context Staff = lower <<
        \set Staff.printPartCombineTexts = ##f
        \clef bass
        \partcombine
        \tenor
        \bass
      >>
    >>
  >>
  \layout {
    \context {
      \Score
      % **** Turns off bar numbering
      \remove "Bar_number_engraver"
    }
    \context {
      \Lyrics
      % **** Prevents lyrics from running too close together
      \override LyricSpace #'minimum-distance = #0.6
      % **** Makes the text of lyrics a little smaller
      \override LyricText #'font-size = #-1
      % **** Moves lines of lyrics closer together
      \override VerticalAxisGroup #'minimum-Y-extent = #'(-1 . 1)
    }
    \context {
      \Staff
      % **** Moves staves a little closer together
      \override VerticalAxisGroup #'minimum-Y-extent = #'(-2 . 2)
    }
  }
}

\markup {
  \fill-line {
    " "
    \column {
      \small \caps \poet % poet
    }
  }
}

%{

The music and words given here are as in the _Hymnal 1940_. I have
been unable to tell for certain what "minor alterations of individual words"
(_The Hymnal 1940 Companion_) were made to Wesley's original text.

There is another set of texts including material from John Cennick. As found in the
_Worship and Service Hymnal_, they read:

Lo! He comes with clouds descending
Once for our salvation slain;
Thousand thousand aints attending,
Swell the triumph of his train:
Hallelujah! Hallelujah! [Hallelujah!]
God appears on earth to reign.

Every eye shall now behold Him,
Robed in dreadful majesty;
Those who set at naught and sold Him,
Pierced and nailed Him to the tree,
Deeply wailing, deeply wailing, [deeply wailing]
Shall the true Messiah see.

Yea, Amen! let all adore Thee,
High on Thine eternal throne;
Saviour, take the power and glory;
Claim the kingdom for Thine own:
O come quickly, O come quickly, [O come quickly]
Hallelujah! Come, Lord, come.

  Change log:
  3-24-06 Move to 2.8, current formatting
%}