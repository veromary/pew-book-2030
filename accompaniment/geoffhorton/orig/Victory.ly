% Version 1.0
% Last edit: April 2, 2006
% The music and words produced by this source code are believed
% to be in the public domain in the United States. The source
% code itself is covered by the Creative Commons Attribution-
% NonCommercial license, 
% http://creativecommons.org/licenses/by-nc/2.5/
% Attribution: Geoff Horton

\version "2.8.0"
\include "english.ly"

#(set-default-paper-size "letter")
#(set-global-staff-size 20)
#(ly:set-option (quote no-point-and-click))

title = "The Strife Is O'er"
composer = "G. P. Sante da Palestrina"
poet = "Latin"
translator = "trans. Francis Pott, alt."
meter = "8 8 8, with Alleluias"
arranger = "adapted by William H. Monk"
piece = "Victory"

world = {
  \key d \major
  \time 3/2
}

refrain = \relative c'' {
  \world
  \oneVoice
  b2\rest \voiceOne fs2 fs |
  g1. |
  fs2 a a |
  b1. |
  a2 a d |
  cs1. |
  d1 \oneVoice b2\rest \voiceOne | \bar "||" \break % end line 1
}

verse = \relative c'' {
  \voiceOne
  a2 a a |
  b1 a2 |
  a( g) fs |
  a1. |
  fs2 fs fs | % end line 2
  fs1 fs2 |
  fs( e) d |
  e1. |
  a2 a a |
  b1 a2 | % end line 3
  a( g) fs |
  a1. \bar "||" |
  \oneVoice b2\rest \voiceOne a2 d |
  cs1. |
  d1. \bar "||"
}


alto = \relative c' {
  \world
  \voiceTwo
  s2 d2 d |
  d1. |
  d2 d d |
  d1. |
  d2 d fs |
  e1. |
  fs1 s2 | % end line 1
  fs2 fs fs |
  g1 fs2 |
  fs2( b,) d |
  e1. |
  d2 d d | % end line 2
  d1 cs2 |
  d( b) b |
  cs1. |
  cs2 fs fs |
  g1 fs2 | % end line 3
  fs2( b,) d |
  e1. |
  s2 d2 fs |
  e1. |
  fs1.
}

tenor = \relative c {
  \world
  \oneVoice
  d2\rest \voiceOne a' a |
  b1. |
  a2 fs fs |
  g1. |
  fs2 a a |
  a1. |
  a1 \oneVoice d,2\rest \voiceOne | % end line 1
  d'2 d d |
  d1 d2 |
  cs( e) d |
  cs1. |
  a2 a a | % end line 2
  b1 a2 |
  a( g) fs |
  a1. |
  a2 d d |
  d1 d2 | % end line 3
  cs2( e) d |
  cs1. | \bar "||"
  \oneVoice d,2\rest \voiceOne d'2 a |
  a1. |
  a1.
}

bass = \relative c, {
  \world
  \voiceTwo
  \once \override NoteHead #'font-size = #-2
  \once \override TextScript #'extra-offset = #'(-5 . 4)
  d2_\markup \tiny \italic "Org."
  d'2 d |
  g,1. |
  d'2 d d |
  g,1. |
  d'2 fs d |
  a1. |
  d1 s2 | % end line 1
  d2 d d |
  g1 d2 |
  fs( e) b' |
  a1. |
  d,2 d d | % end line 2
  b1 fs'2 |
  d2( e) b |
  a1. |
  fs'2 d d |
  g1 d2 | % end line 3
  fs( e) b' |
  a1. |
  s2 fs2 d |
  a1. |
  d1.
}

refrainWords = \lyricmode {
  Al -- le -- lu -- ia! Al -- le -- lu -- ia! Al -- le -- lu -- ia! 
}

verseOne = \lyricmode {
  \set stanza = "1. "
  The strife is o'er, the bat -- tle done,
  The vic -- to -- ry of life is won;
  The song of tri -- umph has be -- gun.
  Al -- le -- lu -- ia!
}

verseTwo = \lyricmode {
  \set stanza = "2. "
  The pow'rs of death have done their worst,
  But Christ their le -- gions hath dis -- persed;
  Let shout of ho -- ly joy out -- burst.
  Al -- le -- lu -- ia!
}

verseThree = \lyricmode {
  \set stanza = "3. "
  The three sad days are quick -- ly sped,
  He ri -- ses glo -- rious from the dead:
  All glo -- ry to our ri -- sen Head!
  Al -- le -- lu -- ia!
}

\markup {
  \column {
    \fill-line { \large \bold \title } % title
    \fill-line { \caps \piece               % piece
	         \caps \composer      % composer
	  }
    \fill-line { \meter          % meter
	         \arranger           % arranger
	  }
  }
}

\score {
  \context ChoirStaff <<
    \context Staff = upper <<
      \context Voice = refrain 
      {
	\refrain  
	\context Voice = verse 
	{
	  \verse
	}
      }
      \context Voice =
         altos { << \alto >> }
      \context Lyrics = one \lyricsto refrain \refrainWords
      \context Lyrics = one \lyricsto verse \verseOne
    >>
    \context Staff = lower <<
      \clef bass
      \context Voice =
        tenors { << \tenor >> }
      \context Voice =
        basses { << \bass >> }
    >>
>>
  \layout {
    \context {
      \Score
      % **** Turns off bar numbering
      \remove "Bar_number_engraver"
    }
    \context {
      \Lyrics
      % **** Prevents lyrics from running too close together
      \override LyricSpace #'minimum-distance = #0.6
      % **** Makes the text of lyrics a little smaller
      \override LyricText #'font-size = #-1
      % **** Moves lines of lyrics closer together
      \override VerticalAxisGroup #'minimum-Y-extent = #'(-1 . 1)
    }
  }
}


\markup { 
  \normalsize {
    \fill-line {
      \hspace #3.0
      \line {
	\bold "4. "
	\column {
	  "He closed the yawning gates of hell,"
	  "The bars from heavn's high portals fell;"
	  "Let hymns of praise his triumphs tell!"
	  \line { \hspace #30 "Alleluia!" }
	}
      }
      \hspace #0.1
      \line {
	\bold "5. "
	\column {
	  "Lord! by the stripes which wounded thee,"
	  "From death's dread sting thy servants free,"
	  "That we may live and sing to thee."
	  \line { \hspace #30 "Alleluia!" }
	}
      }
      \hspace #3.0
    }
  }
}

\markup {
  \fill-line {
    " "
    \column {
      \small \caps \poet % poet
      \small \caps \translator % translator
    }
  }
}



\paper {
  ragged-bottom = ##t
  top-margin = 0.25\in
  bottom-margin = 0.25\in
}

%{
  Source info goes here.
  “  ”   (real quotation marks for easy cut-and-pasting)
  © for cut-and-pasting.
  Revision history: 
  3-23-06 Added licensing info, revised title layout system
%}


