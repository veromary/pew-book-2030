% Version 1.10
% Last edit: March 15, 2006 (Cave Idus Martius!)

\version "2.7.38"
\include "english.ly"

\header {
  subtitle = "Christ is Made the Sure Foundation"
  composer = "Adapted from Henry Purcell"
  poet = "Latin, 7th Cent."
  piece = "Westminster Abbey"
  meter = "87.87.87"
  texttranslator = "John Mason Neale, alt."
}

world = {
  \key g \major
  \time 6/4
}

melody = \relative c'' {
  \world
  b2 d4 b2 g4 |
  c2 a4 fs4 d2 |
  % **** Alternative version of the above measure:
  % c2 a4 fs2 d4 |
  g2 a4 b4.( a8) g4 |
  fs4 e2 d2. |
  fs2 a4 fs2 d4 |
  g2 a4 b2 g4 |
  c2 b4 a4.( b8) g4 |
  g8( fs) fs2 e2. |
  % **** Alternative version of the above measure:
  % fs4 fs2 e2. |
  gs2 gs4 a2 a4 |
  fs2 fs4 g2 g4 | 
  e2 e4 fs2 g4 |
  g2 fs4 g2. \bar "|."
}


alto = \relative c' {
  \world
  d2 d4 d2 d4 |
  e2 e4 d2 a4 |
  d2 c4 b( d) cs |
  d2 cs4 d2. |
  d2 e4 d2 a4 |
  b2 c4 d2 d4 |
  e4( fs8[ g]) fs4 e8( d e4) b |
  c b2 b2. |
  e2 e4 e2 e4 |
  d2 d4 d2 d4 |
  c2 c4 d( c) b |
  a2 a8[ b16 c] b2. |
}

tenor = \relative c' {
  \world
  g2 a4 b2 b8( a) |
  g2 a4 a2 fs4 |
  b2 fs4 g4.( fs8) e4 |
  fs8( g) a4( g) fs2. | 
  a2 a4 a2 fs4 |
  d4.( g8) fs4 g2 b4 |
  g2 a8( b) c4( e,) ds |
  e2 ds4 e2. |
  b'2 b4 a2 a4 |
  a2 a4 g2 g4 |
  g2 a4 a2 g4 |
  fs8( e d4) d4 d2.   
}

bass = \relative c' {
  \world
  g2 fs4 g2 g8( fs) |
  e2 c4 d2 d8( c) |
  b2 a4 g4.( d'8) e4 |
  d4 a2 d2. | 
  d2 cs4 d2 d8[ c] |
  b2 a4 g2 g'4 |
  e2 d4 c2 b4 |
  a b2 e2. |
  e2 d4 cs2 cs4 |
  d2 c!4 b2 b4 |
  c2 a4 d2 g4 |
  d2 d4 g,2.
}

wordsOne = \lyricmode {
  \set stanza = "1. "
  Christ is made the sure foun -- da -- tion,
  Christ the head and cor -- ner -- stone;
  Cho -- sen of the Lord, and pre -- cious,
  Bind -- ing all the Church in one;
  Ho -- ly Zi -- on's help for -- ev -- er
  And her con -- fi -- dence a -- lone.
}

wordsTwo = \lyricmode {
  \set stanza = "2. "
  All that ded -- i -- cat -- ed ci -- ty,
  Dear -- ly loved of God on high,
  in ex -- ul -- tant ju -- bi -- la -- tion
  pours per -- pet -- ual mel -- o -- dy;
  God the One in Three a -- dor -- ing
  in glad hymns e -- ter -- nal -- ly.
}


%{harmonies = \chordmode {
  g1. c2. d2. g2. g2 a4:7
  d4 a2 d2. d1. g1.
  c2. a2.:min b2.:min e2.:min e2. a2.
  d2. g2. c2. d2. d:sus4 g
}
%}

\score {
  <<
%  \context ChordNames \harmonies
    \context ChoirStaff <<
      \context Staff = upper <<
        \context Voice =
           sopranos { \voiceOne << \melody >> }
        \context Voice =
           altos { \voiceTwo << \alto >> }
        \context Lyrics = one \lyricsto sopranos \wordsOne
        \context Lyrics =two \lyricsto sopranos \wordsTwo
      >>
    \context Staff = men <<
      \clef bass
      \context Voice =
        tenors { \voiceOne << \tenor >> }
      \context Voice =
        basses { \voiceTwo << \bass >> }
      >>
    >>
  >>
  \layout {
    \context {
      \Score
      %    \override SeparationItem #'padding = #0.3
      \remove "Bar_number_engraver"
    }
    \context {
      \Lyrics
      % **** Makes the text of lyrics a little smaller
      \override LyricText #'font-size = #-1
    }
  }
}


\markup { 
  \small {
    \fill-line {
      \hspace #3.0
      \column {
        "3. To this temple, where we call thee,"
        "Come, O Lord of Hosts, today;"
        "With thy wonted loving kindness"
        "Hear thy servants as they pray,"
        "And thy fullest benediction"
        "Shed within its walls alway."
      }
      \hspace #3.0
      \column {
        "4. Here vouchsafe to all thy servants"
        "What they ask of thee to gain;"
        "What they gain from thee, for ever"
        "With the blesséd to retain,"
        "And hereafter in thy glory"
        "Evermore with thee to reign."
      }
      \hspace #3.0
    }
  }
}

%{
  From _The Hymnal 1982_, #518. No copyright on words or music is given, so 
  I believe any alterations are long-standing.
  
  The alternative readings of the melody are from Worship III, which likewise
  gives no copyright claim.
%}
