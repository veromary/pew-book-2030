% Version 1.1
% Last edit: March 24, 2006
% The music and words produced by this source code are believed
% to be in the public domain in the United States. The source
% code itself is covered by the Creative Commons Attribution-
% NonCommercial license, 
% http://creativecommons.org/licenses/by-nc/2.5/
% Attribution: Geoff Horton

\version "2.8.0"
\include "english.ly"

#(set-default-paper-size "letter")
#(set-global-staff-size 20)

piece = "St. Catherine"
title = "Faith of Our Fathers"
composer = "Henri Frederic Hemy"
arranger = "James G. Walton"
poet = "Frederick William Faber, alt."
meter = "88.88.88"

world = {
  \key g \major
  \time 3/4
}

melody = \relative c'' {
  \world
  b4 a g |
  g( fs) g |
  a( e) fs |
  g2. |
  g4 fs e | 
  d2 g4 |
  g( fs) g |
  a2. |
  b4 a g |
  g( fs) g |
  a( e) fs |
  g2. |
  g4 fs e |
  d2 b'4 | 
  a( b) a |
  g2. |
  c4^\markup { \small \italic "Refrain" } c c |
  b2 b4 |
  a2 a4 |
  b2. | 
  b4 a g |
  e2 g4 |
  a2 a4 |
  g2. \bar "|."
}


alto = \relative c' {
  \world
  d4 d d |
  d2 d4 |
  e2 d4 |
  d2. |
  e4 d c |
  b2 d4 |
  cs2 cs4 |
  d2. |
  d4 d d |
  d2 d4 |
  e2 d4 |
  b2. |
  e4 d c |
  b2 d4 |
  e2 c4 |
  b2. |
  g'4 g g |
  g2 g4 |
  g2 fs4 |
  g2. |
  b,4 c d | 
  c2 b4 |
  d2 d4 |
  b2.
}

tenor = \relative c' {
  \world
  d4 c b |
  b( c) b |
  c2 a4 |
  b2. |
  g4 g g |
  b2 b4 |
  g2 g4 |
  fs2. |
  g4 c b |
  b( a) g |
  c2 a4 |
  g2. |
  g4 g g |
  g2 g4 | 
  g2 fs4 |
  g2. |
  c4 e c |
  d2 d4 |
  d2 d4 | 
  d2. |
  g,4 g g |
  g2 g4 |
  g2 fs4 |
  g2.
}

bass = \relative c' {
  \world
  g4 g g |
  g( a) g |
  c,2 d4 |
  g,2. |
  c4 c e |
  g2 g4 |
  e2 e4 |
  d2. |
  g4 g g |
  d( c) b |
  a2 d4 |
  e2. |
  c4 c e |
  g2 b,4 |
  c2 d4 |
  g2. |
  e4 c e |
  g2 g4 |
  d2 d4 |
  g2. |
  g,4 a b |
  c2 e4 |
  d2 d4 |
  g,2.
}

wordsOne = \lyricmode {
  \set stanza = "1. " Faith of our fa -- thers! liv -- ing still
  in spite of dun -- geon, fire and sword.
  O how our hearts beat high with joy,
  when -- e'er we hear that glo -- rious word:
  Faith of our fa -- thers, ho -- ly faith!
  We will be true to thee till death.
}

wordsTwo = \lyricmode {
  \set stanza = "2. " Our fa -- thers, chained in pri -- sons dark,
  Were still in heart and con -- science free;
  And tru -- ly blest would be our fate,
  If we, like them, should die for thee.
}

wordsThree = \lyricmode {
  \set stanza = "3. " Faith of our fa -- thers! faith and pray'r
  Shall win all na -- tions un -- to thee;
  And through the truth that comes from God,
  Man -- kind shall then in -- deed be free.
}

\markup {
  \column {
    \fill-line { \large \bold \title } % title
    \fill-line { \caps \piece               % piece
	         \caps \composer      % composer
	  }
    \fill-line { \meter          % meter
	         \arranger           % arranger
	  }
  }
}

\score {
  \context ChoirStaff <<
    \context Staff = upper <<
      \context Voice =
         sopranos { \voiceOne << \melody >> }
      \context Voice =
         altos { \voiceTwo << \alto >> }
      \context Lyrics = one \lyricsto sopranos \wordsOne
      \context Lyrics = two \lyricsto sopranos \wordsTwo
      \context Lyrics = three \lyricsto sopranos \wordsThree
    >>
    \context Staff = men <<
      \clef bass
      \context Voice =
        tenors { \voiceOne << \tenor >> }
      \context Voice =
        basses { \voiceTwo << \bass >> }
    >>
>>
  \layout {
    \context {
      \Score
      % **** Turns off bar numbering
      \remove "Bar_number_engraver"
    }
    \context {
      \Lyrics
      % **** Prevents lyrics from running too close together
      \override LyricSpace #'minimum-distance = #0.6
      % **** Makes the text of lyrics a little smaller
      \override LyricText #'font-size = #-1
      % **** Moves lines of lyrics closer together
      \override VerticalAxisGroup #'minimum-Y-extent = #'(-1 . 1)
    }
  }
}

\markup { 
  \normalsize {
    \fill-line {
      \hspace #3.0
      \line {
	"4. "
	\column {
	  "Faith of our fathers! we will love"
	  "  Both friend and foe in all our strife:"
	  "And preach thee, too, as love knows how,"
	  " By kindly deeds and virtuous life."
	}
      }
      \hspace #3.0
    }
  }
}

\markup {
  \fill-line {
    " "
    \column {
      \small \caps \poet % poet
    }
  }
}

%{
  Per _The Hymnal 1940_, 393.

  The alterations to the lyrics consist in the following:
    First, the last two lines of verse two read:
     "How sweet would be their children's fate,
      If they, like them, could die for thee!"
    The third verse has two versions, one for England and
    one for Ireland. The English version runs thus:
     "Faith of our fathers! Mary's prayers
      Shall win our country back to thee;
      And through the truth that comes from God,
      England shall then indeed be free."
    The Irish version:
     "Faith of our fathers! Mary's prayers
      Shall keep our country fast to thee;
      And through the truth that comes from God
      O we shall prosper and be free!"

  Change log:
  3-24-06 added verses, moved to 2.8 and current formatting
%}