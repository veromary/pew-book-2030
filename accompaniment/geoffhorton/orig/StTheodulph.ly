% Version 1.2
% Last edit: March 25, 2006
% The music and words produced by this source code are believed
% to be in the public domain in the United States. The source
% code itself is covered by the Creative Commons Attribution-
% NonCommercial license, 
% http://creativecommons.org/licenses/by-nc/2.5/
% Attribution: Geoff Horton

\version "2.8.0"
\include "english.ly"

#(set-default-paper-size "letter")
#(set-global-staff-size 20)

title = "All Glory, Laud and Honor"
composer = "Melchior Teschner"
poet = "St. Theodulph"
translator = "trans. John Mason Neale"
piece = "St. Theodulph"
meter = "76.76.D."
arranger = ""

world = {
  \key bf \major
  \time 4/2
  \partial 2*1
}

refrain = \relative c' {
  \world
  bf2^\markup { \small \italic "Refrain" } |
  f' f g a |
  bf1 bf2 d |
  c bf bf a |
  bf1. bf,2 |
  f' f g a |
  bf1 bf2 d |
  c bf bf a |
  bf1. 
  \once \override Score.RehearsalMark #'self-alignment-X = #right
  \once \override Score.RehearsalMark #'break-visibility = #begin-of-line-invisible
  \mark \markup { \small \italic "Fine" } \bar "|." 
}

verse = \relative c'' {
  bf2 |
  d d c bf |
  a g f a |
  bf a g g |
  f1. f2 |
  d f g f |
  f2.( ef4) d2 f |
  ef d c c |
  bf1.
  \once \override Score.RehearsalMark #'self-alignment-X = #right
  \once \override Score.RehearsalMark #'break-visibility = #begin-of-line-invisible
  \mark \markup { \right-align \small \italic "Repeat Refrain" }  \bar "||"
}

alto = \relative c' {
  \world
  bf2 | c bf bf ef | ef( d4 c) d2 f | f4( ef) d2 c c | d1.
  bf2 | c bf bf ef | ef( d4 c) d2 f | f4( ef) d2 c c | d1.
  ef2 | f g4( f) e2 f4( g) | f2 e f f | d4( e) f2 f e2 | f1.
  c2 | bf bf ef d | c( a) bf bf | bf bf bf a | bf1.
}

tenor = \relative c {
  \world
  d2 | f d ef f | f1 f2 bf | a bf g f | f1.
  d2 | f d ef f | g( f4 ef) f2 bf | a bf g f | f1.
  g2 | bf2 bf4( a) g2 d' | c bf a d | bf c d c4( bf) | a1.
  f2 | f bf bf bf | f1 f2 f | g f f2. ef4 | d1.
}

bass = \relative c {
  \world
  bf2 | a bf ef c | bf1 bf2 bf | f' g ef f | bf,1.
  bf2 | a bf ef c | bf1 bf2 bf | f' g ef f | bf,1.
  ef2 | bf bf c d4( e) | f2 c d d | g, a bf c | f1.
  a,2 | bf d ef bf | a( f) bf2 d | ef bf f f | bf1.
}

refrainWords = \lyricmode {
  All glo -- ry, laud, and hon -- or 
  To thee, Re -- deem -- er, King!
  To whom the lips of chil -- dren
  Made sweet ho -- san -- nas ring.
}

verseOne = \lyricmode {
  \set stanza = "1. "
  Thou art the King of Is -- ra -- el,
  Thou Da -- vid's roy -- al Son,
  Who in the Lord's Name com -- est,
  The King and Bless -- ed One.
}

verseTwo = \lyricmode {
  \set stanza = "2. "
  The com -- pa -- ny of an -- \skip 2 gels 
  Are prais -- ing thee on high;
  And mor -- tal men, and all things
  Cre -- a -- ted, make re -- ply.
}

verseThree = \lyricmode {
  \set stanza = "3. "
  The peo -- ple of the He --  \skip 2 brews
  With palms be -- fore thee went;
  Our praise and pray'rs and an -- thems
  Be -- fore thee we pre -- sent.
}

\markup {
  \column {
    \fill-line { \large \bold \title } % title
    \fill-line { \caps \piece               % piece
	         \caps \composer      % composer
	  }
    \fill-line { \meter          % meter
	         \arranger           % arranger
	  }
  }
}

\score {
  \context ChoirStaff <<
    \context Staff = upper <<
       \context Voice = refrain {
         \voiceOne
         \refrain {
           \context Voice = verse {
             \voiceOne
             \verse
           }
         }
       }
      \context Voice =
         altos { \voiceTwo << \alto >> }
      \context Lyrics = one \lyricsto refrain \refrainWords
      \context Lyrics = one \lyricsto verse \verseOne
      \context Lyrics = two \lyricsto verse \verseTwo
      \context Lyrics = three \lyricsto verse \verseThree
    >>
    \context Staff = lower <<
      \clef bass
      \context Voice =
        tenors { \voiceOne << \tenor >> }
      \context Voice =
        basses { \voiceTwo << \bass >> }
    >>
  >>
  \layout {
    \context {
      \Score
      \override VerticalAxisGroup #'remove-first = ##t
      \remove "Bar_number_engraver"
    }
    \context {
      \Lyrics
      \override LyricSpace #'minimum-distance = #0.6
      \override LyricText #'font-size = #-1	
      \override VerticalAxisGroup #'minimum-Y-extent = #'(-1 . 1)
    }
  }
}

% **** You may find that the spacing on the pages of the extra verses
% **** is not to your liking. Add or remove more \hspace #0.1 statements
% **** until you like the results.

\markup { 
  \small {
    \fill-line {
      \hspace #0.1
      \line {
	"4. "
	\column {
	  "To thee before thy passion"
	  "  They sang their hymns of praise;"
	  "To thee, now high exalted,"
	  "  Our melody we raise."
	  \italic "Refrain"
	}
      }
      \line {
	"5. "
	\column {
	  "Thou didst accept their praises;"
	  "  Accept the pray'rs we bring,"
	  "Who in all good delightest,"
	  "  Thou good and gracious King."
	  \italic "Refrain"
	}
      }
      \hspace #0.1
    }
  }
}

\markup {
  \fill-line {
    " "
    \column {
      \small \caps \poet % poet
      \small \caps \translator % translator
    }
  }
}

%{
  Per the _Hymnal 1940_, 62. A variant form of this hymn exists for saints' days,
  lacking the "Repeat Refrain" instruction at the end and setting the whole
  piece as one verse. The words for this are under copyright, so this file
  is not included.

  Also note that the _Hymnal_ regards the refrain as verse 1 and the first verse
  proper as verse 2. I don't see the need for this and changed it.
 
  Change log:
  3-25-06 moved to 2.8 and current formatting.
%}


