﻿
\paper
{
        #(set-paper-size "a4")
	#(define fonts
	 (make-pango-font-tree "LinuxLibertineO"
	 		       "Lucida Sans"
			       "Nimbus Mono"
			       (/ 20 20)))
    %indent = 0.0
    %line-width = 185 \mm
    %between-system-space = 5 \mm
    between-system-padding = #1
    ragged-bottom = ##t
    %top-margin = 0.1 \mm
    %bottom-margin = 0.1 \mm
    %foot-separation = 0.1 \mm
    %head-separation = 0.1 \mm
    %before-title-space = 0.1 \mm
    %between-title-space = 0.1 \mm
    %after-title-space = 0.1 \mm
    %paper-height = 32 \cm
    %print-page-number = ##t
    %print-first-page-number = ##t
    %ragged-last-bottom
    %horizontal-shift
    %system-count
    %left-margin
    %paper-width
    %printallheaders
    %systemSeparatorMarkup
}


%#(set-global-staff-size 23)

\header {
        title = "Salve Regina"
        poet = ""
        meter = ""
        composer = "arr Dom Jean-Hébert Desroquettes"
        arranger = "Officium Completorii"
        tagline = "Transcribed by V. Brandt, 2024 - http://brandt.id.au - This edition may be edited, copied and distributed."
}

global = {
       \key d \major
}

chant = \relative c' {
        \voiceOne
        \time 15/8
        d8 fis a b a4 b8 d cis b a b a a4
        \time 7/8
        d8 a b g~ g e4
        \time 9/8
        fis8 g a fis fis( e) d4 r8 \bar "||"
        \time 6/8
        a'8 b cis d a4 \bar ","
        \time 9/8
        b8 cis d cis b a b a4 \bar "||"
        \time 8/8 
        r8 d8 a b g e fis4
        \time 6/8
        fis8 a b d b a
        \time 9/8
        b8 a g fis e fis e d4 \bar "||"
        \time 12/8
        r8 a' b cis d a b d cis b a4
        \time 4/8
        d8 a b g 
        \time 8/8
        e fis g a g b a a 
        \time 7/8
        g fis e fis( e) d4 \bar "||"
        \time 17/8
        r8 a' b( cis) d~ d cis a b a a b d cis b a4
        \time 15/8
        d,8 a' b d cis b a fis g fis( e) d4. r8 \bar "||"
        \time 8/8
        fis8( g a) fis~ fis d4 r8 \bar "||"
        \time 10/8
        a'8( b cis d cis) b( a) a4 r8 \bar "||"
        \time 11/8
        d8( d d a b g4 e8) fis8( g) a
        \time 7/8
        d, g fis e( d) d4
        \bar "||"
        } 	

 	

alt = \relative c' {
       \voiceTwo
       d2~ d4.~ d2~ d4~ d4.
       cis4 b2.
       d4 b a4.
       r2 r4
       a'4 g4. fis4 e
       d4 cis b d4.~ d4~ d4~ d4~ d4 b cis d~
       d~ d~ d~ d~ d cis
       d8~ d cis b4~ b4 d2~ d4 cis b a
       r8 fis'8~ fis4~ fis4.~ fis4~ fis8 e d4 e4~ e 
       r8 d4~ d~ d~ d b4 a4. s8
       s2 s2
       d4.~ d4~ d cis4.
       d4.~ d d4~ d~ d b~ b a
      }

ten = \relative c' {
       \voiceThree
       a4 g fis4.~ fis4~ fis4 g4 fis4.~ fis4 g2 r4 r4 g4 fis4.
       fis'2 e4~ e d4.~ d4 cis4
       a~ a g b4. g4~ g a4~ a g4~ g fis4~
       fis g a g fis e fis8~ fis4 g~ g~ g2 fis4 a g fis
       r8 d'8~ d4~ d4.~ d4 cis a b cis 
       r8 a4 fis g b~ b fis4. s8
       d'4. cis4 a4. 
       fis4.~ fis4 fis a4.
       a4.~ a4 b4.~ b4 a4 b g fis
       }

bass = \relative c {
       \voiceFour
       d2~ d4. b4 a g d'4.~ d4 e2. fis4 g d4.
       d'2~ d4 cis b4. g4 a
       fis4 d e b4.~ b4 e fis~ fis4 g a, b
       a4 g fis g4~ g a r8 d4 e4~ e b2 d4~ d2~ d4 r8 d'8
       cis4 b4. g4 a fis g a r8 fis4 b,~ b a g d'4. s8
       b'4. a4 fis4. d4. b4 g a4.
       fis4.~ fis4 b4. g4 fis4 e g d'
       }


verba = \lyricmode {
Sal -- ve Re -- gí -- na, ma -- ter mi -- se -- ri -- cór -- di -- ae:
Vi -- ta, dul -- ce -- do, et spes nos -- tra, sal -- ve.
Ad te cla -- ma -- mus, ex -- su -- les fi -- li -- i He -- vae.
Ad te sus -- pi -- ra -- mus, ge -- men -- tes et flen -- tes
in hac la -- cri -- ma -- rum val -- le.
E -- ia er -- go, ad -- vo -- ca -- ta nos --tra
Il -- los tu -- os mi -- se -- ri -- cor -- des o -- cu -- los ad nos con -- ver -- te.
Et Je -- sum, be -- ne -- di -- ctum fru -- ctum ven -- tris tu -- i
no -- bis post hoc ex -- si -- li -- um os -- ten -- de.
O cle -- mens, O pi -- a, O dul -- cis Vir -- go Ma -- ri -- a.
       }


\score {
%        \transpose d c
        <<
	\new ChoirStaff <<
	    \set Score.midiInstrument = "Pipe Organ"
	\new Staff = "plainchant" <<
          \clef treble
          \global 
          \new Voice = "melody" \chant
	  \new Lyrics \lyricsto "melody" \verba
          \new Voice = "alto" << \alt >>
                  >>
	\new Staff = bass <<
          \global
             \clef bass
            \new Voice = "tenors" << \ten >> 
            \new Voice = "bass" << \bass >>
	  >>
        >>
        >>
	\midi { }
	\layout{
            \context {
               \Staff
               \remove "Time_signature_engraver"
               %\remove "Bar_engraver"
               \override BarLine #'X-extent = #'(-1 . 1)
               \override Beam #'transparent = ##t
               \override Stem #'transparent = ##t
               %\override BarLine #'transparent = ##t
               %\override TupletNumber #'transparent = ##t
             }
            \context {
               \Lyrics
               \consists "Bar_engraver"
             }
	}
}




