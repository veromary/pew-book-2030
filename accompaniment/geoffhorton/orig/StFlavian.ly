% Version 1.1
% Last edit: March 24, 2006
% The music and words produced by this source code are believed
% to be in the public domain in the United States. The source
% code itself is covered by the Creative Commons Attribution-
% NonCommercial license, 
% http://creativecommons.org/licenses/by-nc/2.5/
% Attribution: Geoff Horton

\version "2.8.0"
\include "english.ly"

#(set-default-paper-size "letter")
#(set-global-staff-size 20)

title = "Lord, who throughout these forty days"
composer = \markup { "John Day's " \italic " Psalter" }
poet = "Claudia F. Hernaman"
piece = "St. Flavian"
meter = "C.M."
arranger = ""

world = {
  \key f \major
  \time 4/2
  \partial 4*2
}

melody = \relative c' {
  \world
  f2 |
  f e f a |
  g g f f |
  bf a f g |
  a1. a2 |
  a bf c a |
  f g a a |
  g f f e |
  f1. \bar "|."
}


alto = \relative c' {
  \world
  c2 |
  c c c f |
  f e f f |
  f f f d |
  e1. f2 |
  f f e e |
  d e f f |
  e d d c |
  c1.
}

tenor = \relative c' {
  \world
  a2 |
  g g a d |
  d c a a |
  bf c d d |
  cs1. c2 |
  c bf g a |
  a c c c |
  c a g g |
  a1.
}

bass = \relative c {
  \world
  f2 |
  c c f d |
  bf c f f |
  d f bf bf |
  a1. f2 |
  f d c c |
  d c f f |
  c d bf c |
  f1.
}

verseOne = \lyricmode {
  \set stanza = "1. "
  Lord, who through -- out these for -- ty days
  For us didst fast and pray,
  Teach us with thee to mourn our sins,
  And close by thee to stay.
}

verseTwo = \lyricmode {
  \set stanza = "2. "
  As thou with Sa -- tan didst con -- tend,
  And didst the vic -- t'ry win,
  O give us strength in thee to fight,
  In thee to con -- quer sin.
}

\markup {
  \column {
    \fill-line { \large \bold \title } % title
    \fill-line { \caps \piece               % piece
	         \caps \composer      % composer
	  }
    \fill-line { \meter          % meter
	         \arranger           % arranger
	  }
  }
}

\score {
  \context ChoirStaff <<
    \context Staff = upper <<
      \context Voice =
         sopranos { \voiceOne << \melody >> }
      \context Voice =
         altos { \voiceTwo << \alto >> }
      \context Lyrics = one \lyricsto sopranos \verseOne
      \context Lyrics = two \lyricsto sopranos \verseTwo
    >>
    \context Staff = lower <<
      \clef bass
      \context Voice =
        tenors { \voiceOne << \tenor >> }
      \context Voice =
        basses { \voiceTwo << \bass >> }
    >>
  >>
  \layout {
    \context {
      \Score
      % **** Turns off bar numbering
      \remove "Bar_number_engraver"
    }
    \context {
      \Lyrics
      % **** Prevents lyrics from running too close together
      \override LyricSpace #'minimum-distance = #0.6
      % **** Makes the text of lyrics a little smaller
      \override LyricText #'font-size = #-1
      % **** Moves lines of lyrics closer together
      \override VerticalAxisGroup #'minimum-Y-extent = #'(-1 . 1)
    }
  }
}

\markup { 
  \normalsize {
    \fill-line {
      \hspace #1
      \column {
	\line {
	  "3."
	  \column {
	    "As thou didst hunger bear, and thirst,"
	    "  So teach us, gracious Lord,"
	    "To die to self, and chiefly live"
	    "  By thy most holy word."
	  }
	}
	\hspace #1.0
	\line {
	  "4. "
	  \column {
	    "And through these days of penitence"
	    "  And through thy Passiontide,"
	    "Yea, evermore, in life and death,"
	    "  Jesus! with us abide."
	  }
	}
	\hspace #1
	\line {
	  "5. "
	  \column {
	    "Abide with us, that so, this life"
	    "  Of suff'ring overpast,"
	    "An Easter of unending joy"
	    "  We may attain at last!"
	    }
	}
      }
      \hspace #1
    }
  }
}

\markup {
  \fill-line {
    " "
    \column {
      \small \caps \poet % poet
    }
  }
}

%{
  Per _The Hymnal 1940_, #59.
  Change log:
  3-24-06 moved to 2.8, added verses, new formatting
%}