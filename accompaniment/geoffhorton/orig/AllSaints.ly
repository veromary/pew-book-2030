% Version: 1.1
% Last edit: March 23, 2006

\version "2.8.0"
\include "english.ly"

#(set-default-paper-size "letter")
#(set-global-staff-size 18)

title = "Who Are These Like Stars Appearing"
composer = \markup { "Darmstadt " \italic " Gesangbuch"}
poet = "Theobald Heinrich Schenck"
translator = "trans. Francis E. Cox, alt."
piece = "All Saints"
meter = "87.87.77"
arranger = ""

world = {
  \key bf \major
  \time 4/2
}

melody = \relative c'' {
  \world
  bf2 f g g |
  f2. ef4 d2 bf |
  f' g4( a) bf2 a |
  g g f1 |
  bf2 f g g |
  f2. ef4 d2 bf |
  f' g4( a) bf2 a |
  g g f1 |
  c'2 c c bf4( c) |
  d2 c4( bf) c1 |
  bf2 f g4( a) bf2 |
  c c bf1 \bar "|."
}


alto = \relative c' {
  \world
  d2 f ef ef |
  c c bf bf |
  f' f e f |
  f e f1 |
  f2 d ef ef |
  c c bf bf |
  c c c c |
  c2. bf4 a1 |
  f'2 e f f |
  f f f1 |
  f2 d ef f |
  g f4( ef) d1 |
}

tenor = \relative c' {
  \world
  bf2 bf bf c |
  c a bf bf |
  d c c c |
  d c a1 |
  bf2 bf bf c | 
  a f f d |
  d d e d |
  d e f1 |
  a2 g a bf4( a) |
  bf2 bf a1 |
  bf2 bf bf bf |
  bf a bf1 \bar "|."
}

bass = \relative c {
  \world
  bf2 d ef c |
  a f bf bf |
  bf' a g f |
  bf, c f1 |
  d2 bf ef c |
  f a, bf bf |
  a a g a4( bf) |
  c2 c f1 |
  f2 c f d4( c) |
  bf2 d f1 |
  d2 bf ef d |
  ef f bf,1
}

\markup {
  \column {
    \fill-line { \large \bold \title } % title
    \fill-line { \caps \piece               % piece
	         \caps \composer      % composer
	  }
    \fill-line { \meter          % meter
	         \arranger           % arranger
	  }
  }
}

verseOne = \lyricmode {
  \set stanza = "1. "
  Who are these like stars ap -- pear -- ing,
  These, be -- fore God's throne who stand?
  Each a gold -- en crown is wear -- ing;
  Who are all this glo -- rious band?
  Al -- le -- lu -- ia! Hark, they sing,
  Prais -- ing loud their heav'n -- ly King.
}

verseTwo = \lyricmode {
  \set stanza = "2. "
  Who are these of dazz -- ling bright -- ness,
  These in God's own truth ar -- rayed,
  Clad in robes of pur -- est white -- ness,
  Robes whose lus -- ter ne'er shall fade,
  Ne'er be touched by time's rude hand?
  Whence comes all this glo -- rious band?
}

verseThree = \lyricmode {
  \set stanza = "3. "
  These are they who have con -- tend -- ed
  For their Sav -- iour's hon -- or long,
  Wrest -- ling on till life was end -- ed,
  Fol -- l'wing not the sin -- ful throng;
  These, who well the fight sus -- tained,
  Tri -- umph by the Lamb have gained.
}

\score {
  \context ChoirStaff <<
    \context Staff = upper <<
      \context Voice =
         sopranos { \voiceOne << \melody >> }
      \context Voice =
         altos { \voiceTwo << \alto >> }
      \context Lyrics = one {
         \lyricsto sopranos \verseOne
      }
      \context Lyrics = two  {
         \lyricsto sopranos \verseTwo
      }
      \context Lyrics = three {
         \lyricsto sopranos \verseThree
      }
    >>
    \context Staff = lower <<
      \clef bass
      \context Voice =
        tenors { \voiceOne << \tenor >> }
      \context Voice =
        basses { \voiceTwo << \bass >> }
    >>
>>
  \layout {
    \context {
      \Score
      \remove "Bar_number_engraver"
    }
    \context {
      \Lyrics
      \override LyricSpace #'minimum-distance = #0.6
      \override LyricText #'font-size = #-1
      \override VerticalAxisGroup #'minimum-Y-extent = #'(-1 . 1)
    }
  }
}


\markup { 
  \normalsize {
    \fill-line {
      \hspace #3.0
      \line {
	"4. "
	\column {
	  "These are they whose hearts were riven,"
	  "  Sore with woe and anguish tried,"
	  "Who in pray'r full oft have striven:"
	  "  With the God they glorified:"
	  "    Now, their painful conflict o'er,"
	  "    God has bid them weep no more."
	}
      }
      \hspace #3.0
      \line {
	"5. "
	\column {
	  "These, like priests, have watched and waited,"
	  "  Off'ring up to Christ their will,"
	  "Soul and body consecrated,"
	  "  Day and night they serve him still."
	  "    Now in God's most holy place,"
	  "    Blest they stand before his face."
	}
      }
      \hspace #3.0
    }
  }
}

\markup {
  \fill-line {
    " "
    \column {
      \small \caps \poet   % poet
      \small \caps \translator   % translator
    }
  }
}

%{
  Per the _Hymnal 1940 _, 130. The alterations were made by the translator herself.
  Revision history:
   3-23-06 Moved to 2.8, changed titling, outdented extra verses.
%}


