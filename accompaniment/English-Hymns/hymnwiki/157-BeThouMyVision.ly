﻿% ŵ˘ˆ┌ ┐⁀⁀©
% U+0311 = ̑
% U+0361 = ͡
% U + 032E = ̮

%parts and versification finished at 10:22pm on 11 Jul 2006

%The following comment is not being used, as the spirit of the hymn seems to be better with it all as a question:
%changed second, and third verse segments from questions to prayerful statements: 1:02am 21 July 2006
%Wrote the first and second verses on 27 July 2007.  Changed "Be Thou my great Shield, My Sword for the fight;" to "Be Thou my Guardian, My Sword in the fight;" also on 27 July 2007.
%There was another second verse, written in 2006, but this was taken out due to it perhaps being considered a derivative work of a copyrighted translation.

\version "2.10.25"
#(ly:set-option 'point-and-click #f)

\paper
{
	indent = 0.0
	line-width = 192 \mm
	between-system-space = 0.1 \mm
	between-system-padding = #1
	ragged-bottom = ##t
	top-margin = 0.1 \mm
	%ragged-last-bottom
	%bottom-margin
	%foot-separation
	head-separation = 0.1 \mm
	after-title-space = 3.0 \mm
	before-title-space = 0.1 \mm
	%horizontal-shift
	%system-count
	%left-margin
	%paper-width
	paper-height = 30 \cm
	%print-page-number
	%printfirst-page-number
	%between-title-space
	%printallheaders
	%systemSeparatorMarkup
}

\header
{
	%dedication = ""
	title = "Be Thou My Vision"
	%subtitle = ""
	%subsubtitle = ""
	% poet = \markup{ \italic Text: }
	% composer = \markup{ \italic Music: }
	%meter = ""
	%opus = ""
	%arranger = ""
	%instrument = ""
	%piece = \markup{\null \null \null \null \null \null \null \null \null \null \null \null \null \italic Slowly \null \null \null \null \null \note #"4" #1.0 = 70-100}
	%breakbefore
	%copyright = ""
	tagline = \markup{ \tiny \center-align { \line { \italic {Tune name:} Slane (Irish Folk Melody) } \line {\italic Lyrics (verses 3-5): attributed to Dallan Forgaill (8th century) } \line {\italic {Translation (from Old Irish to English) for verses 3-5:} Mary Elizabeth Byrne (1880-1931), 1905 } \line { \italic {Original versification for verses 3-5:} Eleanor Henrietta Hull (1860-1935), 1912} \line { \italic {Arrangement, parts, verses 1-2, and re-versification:} Mark Hamilton Dewey (b. 1980), 2006-2007 } } }
}

\score
{
	\new ChoirStaff
	<<
		\new Staff
		<<
			%\set Score.midiInstrument = "Orchestral Strings"
			%\set Score.midiInstrument = "Choir Aahs"
			\new Voice
			% Soprano Part
			{
				\voiceOne
				\time 3/4
				\key es \major
				%\override Score.MetronomeMark #'transparent = ##t
				\override Score.MetronomeMark #'stencil = ##f
				\tempo 4 = 120
				\repeat volta 5
				{
				es'4 es' f'8 (es') | c'4 bes bes8 (c') | es'4 es' f' g'2. | \break
				f'4 f' f' | f' g' bes' | c'' bes' g'4 | bes'2. | \break
				c''4 c''8 (d''8) es'' (d'') |
				c''4 bes'4 g' | bes' es' d' | c'2( bes4)
			es'4 g' bes' | c''8 (bes'8) g'4 es'8 (g'8) | f'4 es' es' |
				}
				\alternative{ {es'2.\fermata} {es'2.\fermata} }
				\bar "|."
			}

			\new Voice
			% Alto Part
			{
				\voiceTwo
				es'4 es' d'8 (es') c'4 bes bes8 (c') es'4 es' d' es'2.
				d'4 d' d' d' d' g' es' f' g'4 es'2 (g'4)
				aes'4 es'8 (aes') c'' (bes') aes'4 f' es' es' bes bes aes2(
				bes4) bes <<{es' g'} {s es'}>> aes'8 (g') g'4 es'8~ es'8 d'4 es' es'4 es'2.\fermata				
				es'2.\fermata
			}

			\addlyrics
			{
				\set stanza = "1. "
				\override Score . LyricText #'font-size = #-1
Be Thou my Vi -- sion, O Lord of my heart;
Naught be all else to me, Save that Thou art!
Thou my best thought, in the day and the night,
Wa -- king and slee -- ping, Thy pre -- sence my light.

			}
			\addlyrics
			{
				\set stanza = "2. "
			Be Thou my Wis -- dom,
			be Thou my true Word;
			I ev -- er with Thee
			and Thou with me, Lord;
			Thou my great Fath -- er,
			and I Thy true son;
			Thou in me dwel -- ling,
			and I with Thee one.
				
		}
			\addlyrics
			{
				\set stanza = "3. "
				Be Thou my breast -- plate,
				my sword for the fight;
				Be Thou my ar -- mour and
				be Thou my might;
				Thou my soul's Shel -- ter,
				and Thou my high Tow'r:
				Raise Thou me
				\set ignoreMelismata = ##t
				hea- -- ven- -- ward,
				\unset ignoreMelismata
				O Pow'r of my pow'r.
			}
			\addlyrics
			{
				\set stanza = "4. "
				Ri -- ches I heed not,
				nor man's emp -- ty praise,
				Thou mine in -- her -- i -- tance,
				through all my days:
				Thou and Thou on -- ly,
				the first in my heart,
				High King of Hea -- ven,
				my Trea -- sure Thou art.
			}
			\addlyrics
			{
				\set stanza = "5. "
				High King of hea -- ven,
				when bat -- tle is done,
				Grant Heav -- en's joys to me,
				O bright Heav'n's Sun!
				Christ of my own heart,
				what -- ev -- er be -- fall,
				Still be my Vi -- sion,
				O Ru -- ler of \skip 2. all!
			}
		>>


		\new Staff
		<<
			\new Voice
			\clef bass
			% Tenor Part
			{
				\voiceThree
				\time 3/4
				\key es \major
				bes4 bes bes8 (bes) aes4 f f8 (aes) bes4 bes bes bes2.
				bes4 bes bes bes bes bes c'4 d'4 es'4 bes4 (es'2)
				es'4 c'8 (d') es' (d') es'4 d' bes bes g f es2(
				g4) g bes4 bes c'8 (bes) es'4 bes8~ bes bes4 <<{bes4 bes bes2. bes2.} {g4 g g2.\fermata g2.\fermata}>>
			}

			\new Voice
			% Bass Part
			{
				\voiceFour
				g4 g f8 (g) es4 d d8 (es) g4 g f es2.
				bes,4 bes, bes, bes, g, es aes bes4 c'4 g2.
				c'4 aes8 (f) aes (bes) aes4 bes g g es bes, aes,2(
				<<{es4}) {g,4}>> bes, <<{g4} {bes,4}>> g4 aes8 (g) bes4 g8~ g f4 es <<{es4} {bes,4}>>
				es2.\fermata
				%<<{es2.}{bes,2.}{es,2.\fermata}>>
				<\parenthesize \tweak #'font-size #-3 es bes, es,>2.\fermata
			}
		>>
	>>
	
	\layout
	{	
		\context
		{
			\Lyrics
			\override VerticalAxisGroup #'minimum-Y-extent = #'(-0.5 . 0.5)
		}
	}
}
