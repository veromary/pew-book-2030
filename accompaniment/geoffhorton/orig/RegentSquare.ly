% Version 1.2
% Last edit: March 24, 2006
% The music and words produced by this source code are believed
% to be in the public domain in the United States. The source
% code itself is covered by the Creative Commons Attribution-
% NonCommercial license, 
% http://creativecommons.org/licenses/by-nc/2.5/
% Attribution: Geoff Horton

\version "2.8.0"
\include "english.ly"

#(set-default-paper-size "letter")
#(set-global-staff-size 18)

title = "Light's Abode, Celestial Salem"
composer = "Henry Smart"
poet = "Latin, 15th. cent."
translator = "Tr. John Mason Neale, alt."
piece = "Regent Square"
meter = "87.87.87"
arranger = ""

\paper {
  raggedbottom = ##t
}

world = {
  \key bf \major
  \time 4/4
}

melody = \relative c' {
  \world
  \autoBeamOff
  f4 d bf' f |
  d'4. c8 bf4 f |
  g g f bf |
  f ef d2 |
  f4 d bf' f |
  d'4. c8 bf4 a |
  bf a g a8[ bf] |
  a4 g f2 |
  c'4. c8 a4 f |
  d'4. c8 bf4 g |
  ef' d c bf |
  bf a bf2 \bar "|."
}


alto = \relative c' {
  \world
  d4 bf f' d |
  f4. f8 f4 f |
  bf, bf bf bf |
  c a bf2 |
  d4 bf f' f8 ef |
  d4. ef8 d4 d |
  d d d d |
  f e f2 |
  f4. f8 f4 f |
  d4. d8 ef4 ef |
  g f ef d8 ef |
  f4. ef8 d2 
}

tenor = \relative c' {
  \world
  bf4 f f bf |
  bf4. a8 bf4 bf |
  g bf f g |
  f f f2 |
  bf4 bf f f |
  bf4. a8 g4 fs |
  g fs d' c8 b |
  c4 b a2 |
  a4. a8 c4 a |
  bf4. af8 g4 bf |
  c f, g8 a bf4 | 
  c c c2
}

bass = \relative c {
  \world
  bf4 bf d bf |
  f'4. ef8 d4 d |
  ef ef d g, |
  a f bf2 |
  bf'4 f d d8 c |
  bf4. c8 d4 d |
  g d bf g |
  c c f2 |
  f4. f8 f4 f |
  bf,4. bf8 ef4 ef |
  c d ef8 f g4 |
  f f, bf2
}

verseOne = \lyricmode {
  \set stanza = "1. "
  Light's a -- bode ce -- les -- tial Sa -- lem,
  Vi -- sion whence true peace doth spring,
  Bright -- er than the heart can fan -- cy,
  Man -- sion of the high -- est King;
  O how glo -- rious are the prais -- es
  Which of thee the pro -- phets sing!
}

verseTwo = \lyricmode {
  \set stanza = "2. "
  There for ev -- er and for ev -- er 
  Al -- le -- lu -- ia is out -- poured;
  For un -- end -- ing, for un -- bro -- ken
  Is the feast -- day of the Lord;
  All is pure and all is ho -- ly
  That with -- in thy walls is stored.
}

\markup {
  \column {
    \fill-line { \large \bold \title } % title
    \fill-line { \caps \piece               % piece
	         \caps \composer      % composer
	  }
    \fill-line { \meter          % meter
	         \arranger           % arranger
	  }
  }
}

\score {
  \context ChoirStaff <<
    \context Staff = upper <<
      \context Voice =
         sopranos { \voiceOne << \melody >> }
      \context Voice =
         altos { \voiceTwo << \alto >> }
      \context Lyrics = one \lyricsto sopranos \verseOne
      \context Lyrics = two \lyricsto sopranos \verseTwo
    >>
    \context Staff = lower <<
      \clef bass
      \context Voice =
        tenors { \voiceOne << \tenor >> }
      \context Voice =
        basses { \voiceTwo << \bass >> }
    >>
  >>
  \layout {
    \context {
      \Score
      % **** Turns off bar numbering
      \remove "Bar_number_engraver"
    }
    \context {
      \Lyrics
      % **** Prevents lyrics from running too close together
      \override LyricSpace #'minimum-distance = #0.6
      % **** Makes the text of lyrics a little smaller
      \override LyricText #'font-size = #-1
      % **** Moves lines of lyrics closer together
      \override VerticalAxisGroup #'minimum-Y-extent = #'(-1 . 1)
    }
  }
}


\markup { 
  \small {
    \fill-line {
      \hspace #1.0
      \column {
	\line {
	  "3. "
	  \column {
	    "There no cloud or passing vapor"
	    "  Dims the brightness of the air;"
	    "Endless noon-day, glorious noon-day,"
	    "  From the Sun of suns is there;"
	    "There no night brings rest from labor,"
	    "  For unknown are toil and care."
	  }
	}
	\hspace #1.0
	\line {
	  "4. "
	  \column {
	    "O how glorious and resplendent"
	    "  Fragile body, shalt thou be,"
	    "When endued with heav'nly beauty"
	    "  Full of health, and strong, and free"
	    "Full of vigor, full of pleasure"
	    "  That shall last eternally!"
	  }
	}
      }
      \hspace #3.0
      \column {
	\line {
	  "5. "
	  \column {
	    "Now with gladness, now with courage,"
	    "  Bear the burden on thee laid,"
	    "That hereafter these thy labors"
	    "  May with endless gifts be paid,"
	    "And in everlasting glory"
	    "  Thou with brightness be arrayed."
	  }
	}
	\hspace #1.0
	\line {
	  "6. "
	  \column {
	    "Laud and honor to the Father,"
	    "  Laud and honor to the Son,"
	    "Laud and honor to the Spirit,"
	    "  Ever Three, and ever One,"
	    "Consubstantial, co-eternal"
	    "  While unending ages run."
	  }
	}
      }
      \hspace #1.0
    }
  }
}

\markup {
  \fill-line {
    " "
    \column {
      \small \caps \poet % poet
      \small \caps \translator % translator
    }
  }
}

%{
  Per the _Hymnal 1940_
  Change log:
  3-24-06 Move to 2.8, current formatting
%}


