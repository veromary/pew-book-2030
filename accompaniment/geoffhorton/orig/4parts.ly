% Version 2.1
% Last edit: March 16, 2006
% The music and words produced by this source code are believed
% to be in the public domain in the United States. The source
% code itself is covered by the Creative Commons Attribution-
% NonCommercial license, 
% http://creativecommons.org/licenses/by-nc/2.5/
% Attribution: Geoff Horton

% **** This is a template for standard 4-part hymns. It contains most
% **** common options. Comment out the ones you don't need.

\version "2.8.0"
\include "english.ly"

#(set-default-paper-size "letter")
#(set-global-staff-size 20)
#(ly:set-option (quote no-point-and-click))

title = "Template"
composer = "None"
poet = "None"
translator = "None"
meter = "None"
arranger = "None"
piece = "None"
musiccopyright = "None"
textcopyright = "None"

world = {
  \key ef \major
  \time 3/2
  \partial 2*1
}

melody = \relative c' {
  \world

}


alto = \relative c' {
  \world

}

tenor = \relative c' {
  \world

}

bass = \relative c {
  \world

}

verseOne = \lyricmode {
  \set stanza = "1. "

}

\markup {
  \column {
    \fill-line { \large \bold \title } % title
    \fill-line { \caps \piece               % piece
	         \caps \composer      % composer
	  }
    \fill-line { \meter          % meter
	         \arranger           % arranger
	  }
    \fill-line { " " \musiccopyright }
  }
}

\score {
  \context ChoirStaff <<
    \context Staff = upper <<
      \context Voice =
         sopranos { \voiceOne << \melody >> }
      \context Voice =
         altos { \voiceTwo << \alto >> }
      \context Lyrics = one \lyricsto sopranos \verseOne
    >>
    \context Staff = lower <<
      \clef bass
      \context Voice =
        tenors { \voiceOne << \tenor >> }
      \context Voice =
        basses { \voiceTwo << \bass >> }
    >>
>>
  \layout {
    \context {
      \Score
      % **** Turns off bar numbering
      \remove "Bar_number_engraver"
    }
    \context {
      \Lyrics
      % **** Prevents lyrics from running too close together
      \override LyricSpace #'minimum-distance = #0.6
      % **** Makes the text of lyrics a little smaller
      \override LyricText #'font-size = #-1
      % **** Moves lines of lyrics closer together
      \override VerticalAxisGroup #'minimum-Y-extent = #'(-1 . 1)
    }
  }
}


\markup { 
  \normalsize {
    \fill-line {
      \hspace #3.0
      \line {
	\bold "3. "
	\column {
	  
	}
      }
      \hspace #3.0
    }
  }
}

\markup {
  \fill-line {
    " "
    \column {
      \small \caps \poet % poet
      \small \caps \translator % translator
    }
  }
  \fill-line { " " \small \textcopyright }
}



\paper {
  ragged-bottom = ##t
  top-margin = 0.25\in
  bottom-margin = 0.25\in
}

%{
  Source info goes here.
  “  ”   (real quotation marks for easy cut-and-pasting)
  © for cut-and-pasting.
  Revision history: 
  3-23-06 Added licensing info, revised title layout system
%}


