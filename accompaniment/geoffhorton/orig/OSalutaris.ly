% Version 1.20
% Last edit: March 24, 2006
% The music and words produced by this source code are believed
% to be in the public domain in the United States. The source
% code itself is covered by the Creative Commons Attribution-
% NonCommercial license, 
% http://creativecommons.org/licenses/by-nc/2.5/
% Attribution: Geoff Horton

\version "2.8.0"
\include "english.ly"

#(set-default-paper-size "letter")
#(set-global-staff-size 20)

title = "O Salutaris Hostia"
poet = "St. Thomas Aquinas"
composer = "Anthony Werner"
translator = "tr. Edward Caswall"
meter = "L.M."
piece = "Werner"
arranger = ""

melody = \relative c' {
  \key f \major
  \time 3/4
  c4 d c |
  f2 c4 |
  d2 d4 |
  c2. |
  c4 f a |
  g2 e4 |
  f( e) d |
  c2. |
  c4 c c |
  c( f) a |
  bf2 a4 |
  g2. |
  c4 c c |
  c( a) f |
  a2 g4 |
  f2.
  \bar "||"
  \cadenzaOn f2 f 
  \bar "|."
}

alto = \relative c' {
  \key f \major
  \time 3/4
  a4 bf a |
  a( bf) c |
  bf2 bf4 |
  c2. |
  c4 d f8 e |
  d2 c4 |
  d( c) b |
  c2. |
  c4 c c |
  a( c) f8 e |
  d4( e) f |
  f( d e) |
  f e f |
  c( e) d |
  f( d) e |
  c2. |
  d2 c
  \bar "|."
}

tenor = \relative c {
  \key f \major
  \time 3/4
  f4 f f |
  f2 f4 |
  f2 f8 g |
  a2. |
  g4 a c |
  c( b) a |
  a( g) g8 f |
  e2. |
  g4 f e |
  f( a) c |
  bf2 c4 |
  c2. |
  c4 bf a |
  g( c) a |
  c2 c8 bf |
  a2. |
  bf2 a
  \bar "|."
}

bass = \relative c, {
  \key f \major
  \time 3/4
  f4 bf f |
  f( g) a |
  bf( c) d8 e |
  f2. |
  e4 d f, |
  g2 a4 |
  f( g) g |
  c2. |
  e4 d c |
  f( a,) f |
  g2 a8 bf |
  c4( c' bf) |
  a g f |
  e( c) d |
  c2 c4 |
  f2. |
  bf,2 f
  \bar "|."
}

wordsOne = \lyricmode {
  \set stanza = "1. "
  O Sa -- ving Vic -- tim, o --p'ning wide
  The gates of Heav'n to man be -- low,
  Our foes press on from ev -- 'ry side:
  Thine aid sup -- ply, thy strength be -- stow.
}

wordsTwo = \lyricmode {
  \set stanza = "2. "
  To Thy great Name be end -- less praise,
  Im -- mor -- tal God -- head, One in Three;
  O grant us end -- less length of days
  In our true na -- tive land with Thee.
  A -- men.
}

\markup {
  \column {
    \fill-line { \large \bold \title } % title
    \fill-line { \caps \piece               % piece
	         \caps \composer      % composer
	  }
    \fill-line { \meter          % meter
	         \arranger           % arranger
	  }
  }
}

\score {
  \context ChoirStaff <<
    \new Staff  <<
      \context Voice = soprano { \voiceOne \melody }
      \context Voice = alto { \voiceTwo \alto }
      \context Lyrics = one \lyricsto soprano \wordsOne
      \context Lyrics = two \lyricsto soprano \wordsTwo
      >>
    \new Staff <<
      \clef bass
      \context Voice = tenor { \voiceThree \tenor }
      \context Voice = bass { \voiceFour \bass }
      >>
    >>
  \layout {
    \context {
      \Score
      % **** Turns off bar numbering
      \remove "Bar_number_engraver"
    }
    \context {
      \Lyrics
      % **** Prevents lyrics from running too close together
      \override LyricSpace #'minimum-distance = #0.6
      % **** Makes the text of lyrics a little smaller
      \override LyricText #'font-size = #-1
      % **** Moves lines of lyrics closer together
      \override VerticalAxisGroup #'minimum-Y-extent = #'(-1 . 1)
    }
  }
}


\markup { 
  \normalsize {
    \fill-line {
      \hspace #3.0
      \hspace #3.0
      \italic {
	\line {
	  "1. "
	  \column {
	    "O Salutaris Hostia"
	    "Quæ cæli pandis ostium"
	    "Bella premunt hostilia"
	    "Da robur, fer auxilium"
	  }
	}
	\hspace #3.0
	\line {
	  "2. "
	  \column {
	    "Uni Trinoque Domino"
	    "Sit sempiterna gloria"
	    "Qui vitam sine termino"
	    "Nobis donet in patria."
	    "Amen."
	  }
	}
      }
      \hspace #3.0
      \hspace #3.0
    }
  }
}

\markup {
  \fill-line {
    " "
    \column {
      \small \caps \poet % poet
      \small \caps \translator % translator
    }
  }
}

%{
  Change log:
  3-24-06 Moved to 2.8 and current formatting
%}