% Version 1.0
% Last edit: April 2, 2006
% The music and words produced by this source code are believed
% to be in the public domain in the United States. The source
% code itself is covered by the Creative Commons Attribution-
% NonCommercial license, 
% http://creativecommons.org/licenses/by-nc/2.5/
% Attribution: Geoff Horton

\version "2.8.0"
\include "english.ly"

#(set-default-paper-size "letter")
#(set-global-staff-size 20)

title = "Welcome, Happy Morning"
composer = "Arthur Seymour Sullivan"
poet = "Venantius Honorius Fortunatus"
translator = "Trans. John Ellerton"
meter = "11 11.11 11.11"
piece = "Fortunatus"

world = {
  \key g \major
  \time 4/4
}

melody = \relative c'' {
  \world
  \autoBeamOff
  g8[ fs] g4 a b |
  c b a g |
  g2 fs |
  g1 | % end line 1
  b8[ c] d4 e d |
  c b a b |
  a2 cs |
  d1 | % end line 2
  d4 d cs cs |
  b2 fs |
  b4 b a g |
  fs1 | % end line 3
  g4 g fs e |
  a g fs d |
  fs2 e | 
  d1 | % end line 4
  b'8^\markup \small \italic "Refrain." [ c] d4 e d |
  b a g c |
  b2 a |
  g1 \bar "|."
}


alto = \relative c' {
  \world
  d4 d fs g |
  g g e d |
  e2 d |
  d1 | % end line 1
  g4 g g g |
  fs g a g |
  fs2 g |
  fs1 | % end line 2
  fs4 fs fs e |
  d2 d |
  g4 g fs e |
  d1 | % end line 3
  d4 d cs cs |
  d e d d |
  d2 cs |
  d1 | % end line 4
  g4 g g g |
  g fs g a |
  g2 fs |
  g1 | % end line 5
}

tenor = \relative c' {
  \world
  b8[ a] b4 d d |
  e d c b |
  a2 a |
  b1 | % end line 1
  d4 d c d |
  d d d d |
  d2 a |
  a1 | % end line 2
  b4 b as as |
  b2 b |
  b4 b b b |
  b1 | % end line 3
  b4 b a a |
  a a a b |
  a2 g |
  fs1 | % end line 4
  d'4 d c d |
  d c b e |
  d2 c |
  b1 \bar "|."
}

bass = \relative c' {
  \world
  g4 g d g |
  c, g a b |
  c2 d |
  g,1 | % end line 1
  g'8[ a] b4 c b |
  a g fs g |
  a2 a, |
  d1 | % end line 2
  b4 b fs' fs, |
  b2 b |
  b4 b e g |
  b1 | % end line 3
  e,4 e a g |
  fs cs d g, |
  a2 a |
  d1 | % end line 4
  g8[ a] b4 c b |
  g d e c |
  d2 d |
  g,1 
}

verseOne = \lyricmode {
  \set stanza = "1. "
  “Wel -- come, hap -- py morn -- ing!” age to age shall say;
  Hell to -- day is van -- quished, heav'n is won to -- day!
  Lo! the dead is liv -- ing, God for ev -- er -- more!
  Him, their true Cre -- a -- tor, all his works a -- dore!
  “Wel -- come, hap -- py morn -- ing!” age to age shall say.
}

verseTwo = \lyricmode {
  \set stanza = "2. "
  Earth her joy con -- fess -- es, cloth -- ing her for spring,
  All fresh gifts re -- turned with her re -- turn -- ing King:
  Bloom in ev -- 'ry mea -- dow, leaves on ev -- 'ry bough,
  Speak his sor -- row end -- ed, hail his tri -- umph now.
}

verseThree = \lyricmode {
  \set stanza = "3. "
  Months in due suc -- cess -- ion, days of length -- 'ning light,
  Hours and pass -- ing mo -- ments praise thee in their flight.
  Bright -- ness of the morn -- ing, sky and field and sea,
  Van -- quish -- er of dark -- ness, bring their praise to thee.
}

\markup {
  \column {
    \fill-line { \large \bold \title } % title
    \fill-line { \caps \piece               % piece
	         \caps \composer      % composer
	  }
    \fill-line { \meter          % meter
	  }
  }
}

\score {
  \context ChoirStaff <<
    \context Staff = upper <<
      \context Voice =
         sopranos { \voiceOne << \melody >> }
      \context Voice =
         altos { \voiceTwo << \alto >> }
      \context Lyrics = one \lyricsto sopranos \verseOne
      \context Lyrics = two \lyricsto sopranos \verseTwo
      \context Lyrics = three \lyricsto sopranos \verseThree
    >>
    \context Staff = lower <<
      \clef bass
      \context Voice =
        tenors { \voiceOne << \tenor >> }
      \context Voice =
        basses { \voiceTwo << \bass >> }
    >>
>>
  \layout {
    \context {
      \Score
      % **** Turns off bar numbering
      \remove "Bar_number_engraver"
    }
    \context {
      \Lyrics
      % **** Prevents lyrics from running too close together
      \override LyricSpace #'minimum-distance = #0.6
      % **** Makes the text of lyrics a little smaller
      \override LyricText #'font-size = #-1
      % **** Moves lines of lyrics closer together
      \override VerticalAxisGroup #'minimum-Y-extent = #'(-1 . 1)
    }
  }
}


\markup { 
  \normalsize {
    \line {
      \hspace #20
      \bold "4.  "
      \column {
	"Maker and Redeemer, life and health of all,"
	"Thou from heav'n beholding human nature's fall,"
	"Of the Father's Godhead true and only Son,"
	\line { "Manhood to deliver, manhood didst put on." \italic " Refrain" }
      }
    }
  }
}

\markup { 
  \normalsize {
    \line {
      \hspace #20
      \bold "5.  "
      \column {
	"Thou, of life the author, death didst undergo,"
	"Tread the path of darkness, saving strength to show;"
	"Come then, true and faithful, now fulfil thy word,"
	\line { "'Tis thine own third morning! rise, O buried Lord!" \italic " Refrain" }
      }
    }
  }
}

\markup { 
  \normalsize {
    \line {
      \hspace #20
      \bold "6.  "
      \column {
	"Loose the souls long prisoned, bound with Satan's chain;"
	"All that now is fallen raise to life again;"
	"Show thy face in brightness, bid the nations see:"
	\line { "Bring again our daylight: day returns with thee!" \italic " Refrain" }
      }
    }
  }
}

\markup {
  \fill-line {
    " "
    \column {
      \small \caps \poet % poet
      \small \caps \translator % translator
    }
  }
}



\paper {
  ragged-bottom = ##t
  top-margin = 0.25\in
  bottom-margin = 0.25\in
}

%{
  Source info goes here.
  “  ”   (real quotation marks for easy cut-and-pasting)
  © for cut-and-pasting.
  Revision history: 
  3-23-06 Added licensing info, revised title layout system
%}


