% Version 1.20
% Last edit: March 24, 2006
% The music and words produced by this source code are believed
% to be in the public domain in the United States. The source
% code itself is covered by the Creative Commons Attribution-
% NonCommercial license, 
% http://creativecommons.org/licenses/by-nc/2.5/
% Attribution: Geoff Horton

\version "2.8.0"
\include "english.ly"

#(set-default-paper-size "letter")
#(set-global-staff-size 20)

title = "Forty Days and Forty Nights"
composer = \markup { "M.H., in " \italic " Nürnbergisches Gesangbuch" }
poet = "George Hunt Smyttan, alt."
piece = "Heinlein"
meter = "77.77."
arranger = ""

world = {
  \key d \minor
  \time 4/2
}

melody = \relative c'' {
  \world
  a2 a d, e |
  f g a1 |
  a2 a c c |
  b b a1 |
  a2 a bf! bf |
  g g a1 |
  a2 a g f |
  e e d1 \bar "|."
}


alto = \relative c' {
  \world
  d2 d a e' |
  d d  cs1 |
  d2 d e e |
  e e cs1 |
  d2 d d d |
  c c c1 |
  c2 f e d |
  d cs d1 \bar "|."
}

tenor = \relative c {
  \world
  f2 f f a |
  a d, e1 |
  f2 f a a |
  a gs a1 |
  f2 f g g |
  e e f1 |
  a2 c c a |
  a a f1 \bar "|."
}

bass = \relative c {
  \world
  d2 d d cs |
  d bf a1 |
  d2 d a a |
  e' e a,1 |
  d2 d g, g |
  c c f,1 |
  f'2 f c d |
  a a d1 \bar "|."
}

verseOne = \lyricmode {
  \set stanza = "1. "
  For -- ty days and for -- ty nights
  Thou wast fast -- ing in the wild;
  For -- ty days and for --ty nights
  Tempt -- ed and yet un -- de -- filed.
}

verseTwo = \lyricmode {
  \set stanza = "2. "
  Shall not we thy sor -- row share
  And from world -- ly joys ab -- stain
  Fast -- ing with un -- ceas -- ing pray'r,
  Strong with thee to suf -- fer pain?
}

\markup {
  \column {
    \fill-line { \large \bold \title } % title
    \fill-line { \caps \piece               % piece
	         \caps \composer      % composer
	  }
    \fill-line { \meter          % meter
	         \arranger           % arranger
	  }
  }
}

\score {
  \context ChoirStaff <<
    \context Staff = upper <<
      \context Voice =
         sopranos { \voiceOne << \melody >> }
      \context Voice =
         altos { \voiceTwo << \alto >> }
      \context Lyrics = one \lyricsto sopranos \verseOne
      \context Lyrics = two \lyricsto sopranos \verseTwo
    >>
    \context Staff = lower <<
      \clef bass
      \context Voice =
        tenors { \voiceOne << \tenor >> }
      \context Voice =
        basses { \voiceTwo << \bass >> }
    >>
  >>
  \layout {
    \context {
      \Score
      % **** Turns off bar numbering
      \remove "Bar_number_engraver"
    }
    \context {
      \Lyrics
      % **** Prevents lyrics from running too close together
      \override LyricSpace #'minimum-distance = #0.6
      % **** Makes the text of lyrics a little smaller
      \override LyricText #'font-size = #-1
      % **** Moves lines of lyrics closer together
      \override VerticalAxisGroup #'minimum-Y-extent = #'(-1 . 1)
    }
  }
}


\markup { 
  \normalsize {
    \fill-line {
      \hspace #1.0
      \line {
	"3. "
	\column {
	  "Then if Satan on us press,"
	  "  Flesh or spirit to assail,"
	  "Victor in the wilderness,"
	  "  Grant we may not faint nor fail!"
	  }
	}
      \hspace #1.0
      \line {
	"4. "
	\column {
	  "So shall we have peace divine:"
	  "  Holier gladness ours shall be;"
	  "Round us, too, shall angels shine,"
	  "  Such as ministered to thee."
	}
      }
      \hspace #1.0
      \line {
	"5. "
	\column {
	  "Keep, O keep us, Saviour dear,"
	  "  Ever constant by thy side;"
	  "That with thee we may appear"
	  "  At the-eternal Eastertide."
	}
      }
      \hspace #1.0
    }
  }
}

\markup {
  \fill-line {
    " "
    \column {
      \small \caps \poet % poet
    }
  }
}

%{
  Per _The Hymnal 1940_, 55.
  Change log:
  3-24-06 Move to 2.8, current formatting
%}
