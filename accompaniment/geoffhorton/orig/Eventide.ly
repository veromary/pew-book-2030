% Version: 1.2
% Last edit: March 24, 2006
% The music and words produced by this source code are believed
% to be in the public domain in the United States. The source
% code itself is covered by the Creative Commons Attribution-
% NonCommercial license, 
% http://creativecommons.org/licenses/by-nc/2.5/
% Attribution: Geoff Horton

\version "2.8.0"
\include "english.ly"

#(set-default-paper-size "letter")
#(set-global-staff-size 20)

piece = "Eventide"
title = "Abide With Me"
composer = "William H. Monk"
meter = "10.10.10.10"
poet = "Henry Francis Lyte"
arranger = ""

world = {
  \key ef \major
  \time 4/4
}

melody = \relative c'' {
  \world
  g2 g4 f | 
  ef2 bf' | 
  c4 bf bf af |
  g1 |
  g2 af4 bf |
  c2 bf |
  af4 f g a |
  bf1 |
  g2 g4 f |
  ef2 bf' | 
  bf4 af af g |
  f1 |
  f2 g4 af |
  g f ef af |
  g2 f | 
  ef1 \bar "|."
}


alto = \relative c' {
  \world
  ef2 d4 d |
  ef2 ef |
  ef4 d ef f |
  ef1 |
  ef2 ef4 ef |
  ef2 ef |
  ef4 f ef ef |
  d1 |
  ef2 d4 d | 
  ef2 ef |
  ef4 ef e e |
  f1 |
  d2 ef4 d |
  ef d ef f |
  ef2 d |
  ef1
}

tenor = \relative c' {
  \world
  bf2 bf4 af |
  g2 ef |
  ef4 bf' bf bf |
  bf1 |
  bf2 af4 g |
  af2 g |
  c4 bf bf ef, |
  f1 |
  g4( af) bf af |
  g2 ef'4( d) |
  c4 c c bf |
  af1 |
  bf2 bf4 bf |
  bf af g c |
  bf2. af4 |
  g1
}

bass = \relative c {
  \world
  ef2 bf4 bf |
  c2 g |
  af4 bf c d |
  ef1 |
  ef4( d) c bf |
  af2 ef' |
  f4 d ef c |
  bf1 |
  ef2 bf4 bf |
  c2 g |
  af4. bf8 c4 c |
  f1 |
  af2 g4 f |
  ef bf c af |
  bf2 bf |
  ef1
}

verseOne = \lyricmode {
  \set stanza = "1. "
  A -- bide with me: fast falls the e -- ven -- tide;
  The dark -- ness deep -- ens; Lord, with me a -- bide:
  When oth -- er help -- ers fail and com -- forts flee,
  Help of the help -- less, O a -- bide with me.
}

verseTwo = \lyricmode {
  \set stanza = "2. "
  Swift to its close ebbs out life's lit -- tle day,
  Earth's joys grow dim, its glo -- ries pass a -- way,
  Change and de -- cay in all a -- round I see;
  O thou who chang -- est not, a -- bide with me.
}

verseThree = \lyricmode {
  \set stanza = "3. "
  I need thy pres -- ence ev -- 'ry pass -- ing hour;
  What but thy grace can foil the temp -- ter's pow'r?
  Who, like thy -- self, my guide and stay can be?
  Through cloud and sun -- shine, Lord, a -- bide with me.
}

\markup {
  \column {
    \fill-line { \large \bold \title } % title
    \fill-line { \caps \piece               % piece
	         \caps \composer      % composer
	  }
    \fill-line { \meter          % meter
	         \arranger           % arranger
	  }
  }
}

\score {
  \context ChoirStaff <<
    \context Staff = upper <<
      \context Voice =
         sopranos { \voiceOne << \melody >> }
      \context Voice =
         altos { \voiceTwo << \alto >> }
      \context Lyrics = one \lyricsto sopranos \verseOne
      \context Lyrics = two \lyricsto sopranos \verseTwo
      \context Lyrics = three \lyricsto sopranos \verseThree
    >>
    \context Staff = men <<
      \clef bass
      \context Voice =
        tenors { \voiceOne << \tenor >> }
      \context Voice =
        basses { \voiceTwo << \bass >> }
    >>
  >>
  \layout {
    \context {
      \Score
      % **** Turns off bar numbering
      \remove "Bar_number_engraver"
    }
    \context {
      \Lyrics
      % **** Prevents lyrics from running too close together
      \override LyricSpace #'minimum-distance = #0.6
      % **** Makes the text of lyrics a little smaller
      \override LyricText #'font-size = #-1
      % **** Moves lines of lyrics closer together
      \override VerticalAxisGroup #'minimum-Y-extent = #'(-1 . 1)
    }
  }
}

\markup { 
  \small {
    \fill-line {
      \line {
	"4. "
	\column {
	  "I fear no foe, with thee at hand to bless;"
	  "Ills have no weight, and tears no bitterness."
	  "Where is death's sting? where, grave, thy victory?"
	  "I triumph still, if thou abide with me."
	}
      }
%      \hspace #3.0
      \line {
	"5. "
	\column {
	  "Hold thou thy cross before my closing eyes;"
	  "Shine through the gloom, and point me to the skies;"
	  "Heav'n's morning breaks, and earth's vain shadows flee:"
	  "In life, in death, O Lord, abide with me."
	}
      }
    }
  }
}

\markup {
  \fill-line {
    " "
    \column {
      \small \caps \poet % poet
    }
  }
}

%{
  Per _Hymnal 1940, 467
  Change log:
  3-24-06 moved to 2.8 and new formatting
%}
