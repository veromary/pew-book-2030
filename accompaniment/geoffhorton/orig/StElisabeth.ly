% Version 1.1
% Last edit: March 24, 2006
% The music and words produced by this source code are believed
% to be in the public domain in the United States. The source
% code itself is covered by the Creative Commons Attribution-
% NonCommercial license, 
% http://creativecommons.org/licenses/by-nc/2.5/
% Attribution: Geoff Horton

\version "2.8.0"
\include "english.ly"

#(set-default-paper-size "letter")
#(set-global-staff-size 20)

title = "Fairest Lord Jesus"
composer = "Silesian Melody"
poet = "Münster Gesangbuch"
piece = "St. Elisabeth"
meter = "568.558"
arranger = "harm. T. Tertius Noble"
translator = "trans. Anon."


world = {
  \key ef \major
  \time 4/4
}

melody = \relative c' {
  \world
  ef2 ef4 ef |
  f( d) ef2 |
  g4 g g g |
  af4( f) g2 |
  bf2 ef4 c |
  bf2 af4( g) |
  af2 g |
  f2. \tweak #'padding #'-3 r4 |
  bf2 c4 bf |
  bf( g) af2 |
  af2 bf4 af |
  af( f) g \bar "" \breathe g |
  g g bf af |
  g2 f2 |
  ef1 \bar "|."
}


alto = \relative c' {
  \world
  ef4( d) c bf |
  af( bf) bf2 |
  g'4 f ef d |
  c( bf) bf2 |
  ef4( d) ef ef |
  d( ef) ef2 |
  ef4( f) f( ef) |
  ef2( d4) s4 |
  ef4( d) ef4 d |
  ef2 ef4( g) |
  f2 f4 ef |
  d2 d4 ef |
  g f ef ef |
  d( ef) c( d) |
  bf1
}

tenor = \relative c' {
  \world
  g2 af4 bf |
  c( af) af( g) |
  bf4 bf c d |
  ef( d) bf2 |
  g4( af) bf c |
  af( bf) c( bf) |
  c( bf) bf2 |
  bf2. r4 |
  bf2 af4 af |
  g( bf) c2 |
  c2 bf4 c4 |
  f,( bf) bf c |
  bf bf g af |
  bf2 af4( f) |
  g1
}

bass = \relative c {
  \world
  ef2 af4 g |
  f2 ef |
  ef4 d c bf |
  af( bf) ef( f) |
  g( f) g af |
  f( g) af( ef) |
  c( d) ef( g) |
  bf2. s4 |
  g2 f4 f |
  ef4.( d8) c2 |
  f4( ef) d c |
  bf4.( af8) g4 c |
  ef d c c |
  bf( g) af( bf) |
  ef1
}

altbass = \relative c {
  s1 s1 s1 s1 |
  s1 s1 s2.
  \override NoteColumn #'force-hshift = #0.2
  \tiny g4 |
  bf c bf af |
  g f
}

verseOne = \lyricmode {
  \set stanza = "1. "
  Fair -- est Lord Je -- sus,
  Rul -- er of all na -- ture,
  O thou of God and man the Son;
  Thee will I cher -- ish,
  Thee will I hon -- or,
  Thou, my soul's glo -- ry, joy, and crown.
}

verseTwo = \lyricmode {
  \set stanza = "2. "
  Fair are the mea -- dows,
  Fair -- er still the wood -- lands,
  Robed in the bloom -- ing garb of spring:
  Je -- sus is fair -- er,
  Je -- sus is pur -- er,
  Who makes the woe -- ful heart to sing.
}

verseThree = \lyricmode {
  \set stanza = "4. "
  Fair is the sun -- shine,
  Fair -- er still the moon -- light,
  And all the twink -- ling star -- ry hosts:
  Je -- sus shines bright -- er,
  Je -- sus shines pur -- er,
  Than all the an -- gels heav'n can boast.
}

\markup {
  \column {
    \fill-line { \large \bold \title } % title
    \fill-line { \caps \piece               % piece
	         \caps \composer      % composer
	  }
    \fill-line { \meter          % meter
	         \arranger           % arranger
	  }
  }
}

\score {
  \context ChoirStaff <<
    \context Staff = upper <<
      \context Voice =
         sopranos { \voiceOne << \melody >> }
      \context Voice =
         altos { \voiceTwo << \alto >> }
      \context Lyrics = one \lyricsto sopranos \verseOne
      \context Lyrics = two \lyricsto sopranos \verseTwo
      \context Lyrics = three \lyricsto sopranos \verseThree
    >>
    \context Staff = lower <<
      \clef bass
      \context Voice =
        tenors { \voiceOne << \tenor >> }
      \context Voice =
        basses { \voiceTwo << \bass >> }
      \context Voice =
        extra { \voiceFour << \altbass >> }
    >>
  >>
  \layout {
    \context {
      \Score
      % **** Turns off bar numbering
      \remove "Bar_number_engraver"
    }
    \context {
      \Lyrics
      % **** Prevents lyrics from running too close together
      \override LyricSpace #'minimum-distance = #0.6
      % **** Makes the text of lyrics a little smaller
      \override LyricText #'font-size = #-1
      % **** Moves lines of lyrics closer together
      \override VerticalAxisGroup #'minimum-Y-extent = #'(-1 . 1)
    }
  }
}

\markup {
  \fill-line {
    " "
    \column {
      \small \caps \poet % poet
      \small \caps \translator % translator
    }
  }
}

\paper {
  ragged-bottom = ##t
  top-margin = 0.25\in
  bottom-margin = 0.25\in
}

%{
  Per _The Hymnal 1940_, #346, 2nd tune. The 1918 copyright has expired.
  Change log:
  3-24-06 Move to 2.8, new formatting
%}


