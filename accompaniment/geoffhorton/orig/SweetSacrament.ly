% Version 1.11
% Last edit: March 25, 2006
% The music and words produced by this source code are believed
% to be in the public domain in the United States. The source
% code itself is covered by the Creative Commons Attribution-
% NonCommercial license, 
% http://creativecommons.org/licenses/by-nc/2.5/
% Attribution: Geoff Horton

\version "2.8.0"
\include "english.ly"

#(set-default-paper-size "letter")
#(set-global-staff-size 20)
#(ly:set-option (quote no-point-and-click))

title = "Jesus, My Lord, My God, My All"
composer = \markup \italic "Romischkatholisches Gesangbuchlein"
poet = "Frederick W. Faber"
piece = "Sweet Sacrament"
meter = "L.M., with Refrain"
arranger = ""

world = {
  \key d \major
  \time 3/4
}

melody = \relative c'' {
  \world
  a4 fs d |
  d( cs) d |
  e( a) g |
  g( fs2) |
  fs4 e d |
  g2 e4 |
  d2 cs4 |
  d2. |
  a'4 fs d |
  d( cs) d |
  e( a) g |
  g( fs2) |
  fs4 e d |
  g2 e4 |
  d2 cs4 |
  d2. \bar "||"
  a'4 a a |
  b2 b4 |
  cs2 cs4 |
  d2. |
  a4 g fs | 
  b2 g4 |
  e( a) g |
  g( fs2) |
  fs4 e d |
  g2 e4 |
  d2 cs4 |
  d2. \bar "|."
}


alto = \relative c' {
  \world
  d4 d d |
  a2 a4 |
  a4( cs) e |
  e4( d2) |
  d4 cs b |
  b2 b4 |
  a( b) a |
  a2. |
  d4 d d |
  a2 a4 |
  a( cs) e |
  d2. |
  d4 cs d |
  b2 b4 |
  a( b) a |
  a2. |
  d4 cs d |
  d2 e4 |
  g( fs) e |
  d2. |
  d4 e d |
  d2 e4 |
  d( cs) e |
  e( d2) |
  d4 cs b |
  b2 b4 |
  b( a) a |
  a2.
}

tenor = \relative c {
  \world
  fs4 a a | 
  g2 fs4 |
  a2 a4 |
  a2. |
  a4 a fs |
  e( fs) g |
  fs( g) e |
  fs2. |
  fs4 a g |
  a( g) fs |
  a2 a4 |
  a2. |
  a4 g a |
  g2 g4 |
  fs4( g) e |
  fs2. |
  fs4 g a |
  g( a) g |
  b( a) g |
  fs( b2) |
  a4 a a |
  g( a) b |
  a2 a4 |
  a2. |
  a4 a fs |
  e( fs) g |
  g( fs) e |
  fs2. 
}

bass = \relative c {
  \world
  d4 d fs |
  e2 d4 |
  cs( a) cs |
  d2. |
  d4 a b |
  e2 g,4 |
  a2 a4 |
  d2. |
  d4 d e |
  fs( e) d |
  cs( a) cs |
  d2. |
  d4 e fs |
  e( e,) g |
  a2 a4 |
  d2. |
  d4 e fs | 
  g( fs) e |
  a,2 a4 |
  b2. |
  fs'4 cs d |
  g( fs) e |
  a( a,) cs |
  d2. |
  d4 a b |
  e2 g,4 |
  a2 a4 |
  d2.
}

verseOne = \lyricmode {
  \set stanza = "1. "
  Je -- sus, my Lord, my God, my All,
  How can I love thee as I ought?
  And how re -- vere this won -- drous gift,
  So far sur -- pass -- ing hope or thought?
  Sweet Sac -- ra -- ment, we thee a -- dore!
  O make us love thee more and more!
  O make us love thee more and more!
}

verseTwo = \lyricmode {
  \set stanza = "2. "
  Had I but Ma -- ry's sin -- less heart,
  To love thee with, my dear -- est King;
  O! with what bursts of fer -- vent praise,
  Thy good -- ness, Je -- sus, would I sing.
}

\markup {
  \column {
    \fill-line { \large \bold \title } % title
    \fill-line { \caps \piece               % piece
	         \caps \composer      % composer
	  }
    \fill-line { \meter          % meter
	         \arranger           % arranger
	  }
  }
}


\score {
  \context ChoirStaff <<
    \context Staff = upper <<
      \context Voice =
         sopranos { \voiceOne << \melody >> }
      \context Voice =
         altos { \voiceTwo << \alto >> }
      \context Lyrics = one \lyricsto sopranos \verseOne
      \context Lyrics = two \lyricsto sopranos \verseTwo
    >>
    \context Staff = lower <<
      \clef bass
      \context Voice =
        tenors { \voiceOne << \tenor >> }
      \context Voice =
        basses { \voiceTwo << \bass >> }
    >>
  >>
  \layout {
    \context {
      \Score
      % **** Turns off bar numbering
      \remove "Bar_number_engraver"
    }
    \context {
      \Lyrics
      % **** Prevents lyrics from running too close together
      \override LyricSpace #'minimum-distance = #0.6
      % **** Makes the text of lyrics a little smaller
      \override LyricText #'font-size = #-1
      % **** Moves lines of lyrics closer together
      \override VerticalAxisGroup #'minimum-Y-extent = #'(-1 . 1.5)
    }
  }
}


\markup {
  \fill-line {
    " "
    \column {
      \small \caps \poet % poet
    }
  }
}

%{
  As found in _Worship III_. The third verse ("O! See upon . . . .") is from the 
  1955 Mediator Dei hymnal and is under copyright, and therefore not included
  here.

  Change log:
  3-25-06 moved to 2.8 and current formatting
  4-4-06 removed point-and-click from the PDF
%}


