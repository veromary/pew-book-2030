% Version 1.0
% Last edit: March 26, 2006
% The music and words produced by this source code are believed
% to be in the public domain in the United States. The source
% code itself is covered by the Creative Commons Attribution-
% NonCommercial license, 
% http://creativecommons.org/licenses/by-nc/2.5/
% Attribution: Geoff Horton

\version "2.8.0"
\include "english.ly"

#(set-default-paper-size "letter")
#(set-global-staff-size 20)

title = "When Jesus Left His Father's Throne"
composer = "R. Vaughn Williams"
poet = "James Montgomery"
meter = "C.M.D."
arranger = ""
piece = "Kingsfold"

world = {
  \key e \minor
  \time 4/4
  \partial 4*1
}

melody = \relative c'' {
  \world
  \autoBeamOff
  g8[ fs] |
  e4 e e d |
  g g a g8[ a] |
  b4 b a8[ g] e4 |
  d2. % end line 1
  g8[ fs] |
  e4 e e d |
  g g a g8[ a] |
  b4 b a8[ g] e4 |
  e2. % end line 2
  b'8[ c] |
  d4 b b8[ a] g4 |
  a a b g8[ a] |
  b4 b8[ a] g4 e |
  d2. % end line 3
  g8[ fs] |
  e4 e e8[ d] e[ fs] |
  g4 g a g8[ a] |
  b4 b a8[ g] e4 |
  e2. \bar "|."
}


alto = \relative c' {
  \world
  e4 |
  b b c a |
  d b d d8[ e] |
  g4 g e c |
  a2. % end line 1
  d4 |
  d c c8[ b] a4 |
  d cs d d8[ c] |
  b4 d c c |
  b2. % end line 2
  e4 |
  fs fs g8[ d] d4 |
  e d d d8[ fs] |
  g4 g8[ fs] d4 c8[ b] |
  a2. % end line 3
  d4 |
  d d c c |
  d d8[ cs] d4 d8[ c] |
  b4 d e c |
  b2. \bar "|."
}

tenor = \relative c' {
  \world
  b8[ a] |
  g4 g g fs |
  g g fs g8[ c] |
  b4 d c g |
  g2( fs4) % end line 1
  g4 |
  g g g fs |
  g g g g8[ fs] |
  g4 g e e8[ g] |
  g2. % end line 2
  g4 |
  b d d8[ c] b4 |
  a8[ g] fs4 g d'8[ c] |
  b4 d8[ c] b4 g |
  g2( fs4) % end line 3
  g4 |
  g g a c |
  b8[ a] g4 fs g8[ fs] |
  g4 fs e e8[ fs] |
  g2. \bar "|."
}

bass = \relative c {
  \world
  e4 |
  e e8[ d] c4 c |
  b e d b8[ a] |
  g4 g a c |
  d2. % end line 1
  b4 |
  c c8[ b] a4 d |
  b e d8[ c] b[ a] |
  g4 b c a |
  e'2. % end line 2
  e4 |
  b b' e,8[ fs] g4 |
  c, d g b,8[ a] |
  g4 g8[ a] b4 c |
  d2. % end line 3
  b4 |
  c c8[ b] a4 a' |
  g8[ fs] e4 d b8[ a] |
  g4 b c a |
  <e' \tweak #'font-size #-2 e,>2. \bar "|."
}

verseOne = \lyricmode {
  \set stanza = "1. "
  When Je -- sus left his Fa -- ther's throne,
  He chose a hum -- ble birth;
  Like us, un -- hon -- ored and un -- known,
  He came to dwell on earth.
  Like him may we be found be -- low,
  In wis -- dom's path of peace;
  Like him in grace and know -- ledge grow,
  As years and strength in -- crease.
}

verseTwo = \lyricmode {
  \set stanza = "2. "
  Sweet were his words and kind his look,
  When mo -- thers round him pressed;
  Their in -- fants in his arms he took,
  And on his bo -- som blessed.
  Safe from the world's al -- lur -- ing harms,
  Be -- neath his watch -- ful eye,
  Thus in the cir -- cle of his arms
  May we for ev -- er lie.
}

verseThree = \lyricmode {
  \set stanza = "3. "
  When Je -- sus in -- to Si -- on rode,
  The chil -- dren sang a -- round;
  For joy they plucked the palms and strowed
  Their gar -- ments on the ground.
  Ho -- san -- na our glad voic -- es raise,
  Ho -- san -- na to our King;
  Should we for -- get our Sa -- viour's praise,
  The stones them -- selves would sing.
}

\markup {
  \column {
    \fill-line { \large \bold \title } % title
    \fill-line { \caps \piece               % piece
	         \caps \composer      % composer
	  }
    \fill-line { \meter          % meter
	         \arranger           % arranger
	  }
  }
}

\score {
  \context ChoirStaff <<
    \context Staff = upper <<
      \context Voice =
         sopranos { \voiceOne << \melody >> }
      \context Voice =
         altos { \voiceTwo << \alto >> }
      \context Lyrics = one \lyricsto sopranos \verseOne
      \context Lyrics = two \lyricsto sopranos \verseTwo
      \context Lyrics = three \lyricsto sopranos \verseThree
    >>
    \context Staff = lower <<
      \clef bass
      \context Voice =
        tenors { \voiceOne << \tenor >> }
      \context Voice =
        basses { \voiceTwo << \bass >> }
    >>
>>
  \layout {
    \context {
      \Score
      % **** Turns off bar numbering
      \remove "Bar_number_engraver"
    }
    \context {
      \Lyrics
      % **** Prevents lyrics from running too close together
      \override Lyricspace #'minimum-distance = #0.6
      % **** Makes the text of lyrics a little smaller
      \override LyricText #'font-size = #-1
      % **** Moves lines of lyrics closer together
      \override VerticalAxisGroup #'minimum-Y-extent = #'(-1 . 1)
    }
  }
}


\markup { 
  \normalsize {
    \fill-line {
      \column {

      }
      \hspace #3.0
      \column {

      }
    }
  }
}

\markup {
  \fill-line {
    " "
    \column {
      \small \caps \poet % poet
    }
  }
}



\paper {
  ragged-bottom = ##t
  top-margin = 0.25\in
  bottom-margin = 0.25\in
}

%{
  Per _The Hymnal 1940_, #331.
  The accompaniment was first published in _The English Hymnal_ of 1906,
  so it is now out of copyright. The alterations of the lyrics consist
  of omitted portions of stanzas.

  3-26-06 New hymn.
%}


