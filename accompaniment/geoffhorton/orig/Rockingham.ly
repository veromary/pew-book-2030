% Version 1.20
% Last edit: March 24, 2006
% The music and words produced by this source code are believed
% to be in the public domain in the United States. The source
% code itself is covered by the Creative Commons Attribution-
% NonCommercial license, 
% http://creativecommons.org/licenses/by-nc/2.5/
% Attribution: Geoff Horton

\version "2.8.0"
\include "english.ly"

#(set-default-paper-size "letter")
#(set-global-staff-size 20)

title = "When I Survey the Wondrous Cross"
composer = "Unknown"
arranger = "Arr. Edward Miller"
poet = "Isaac Watts"
piece = "Rockingham"
meter = "L.M."

world = {
  \key d \major
  \time 3/4
  \partial 4*1
}

melody = \relative c' {
  \world
  d4 |
  fs( g) e |
  d2 fs4 |
  a2 b4 |
  a2 a4 |
  d2 cs4 |
  b2 a4 |
  a( g) fs |
  fs( e) e |
  a2 b4 |
  cs2 a4 |
  d( fs,) gs |
  a2 d,4 |
  g2 fs4 |
  e2 d4 |
  d8( e fs4) e |
  d2 \bar "|."
}


alto = \relative c' {
  \world
  d4 |
  d2 cs4 |
  d2 d4 |
  d2 d4 |
  d2 d4 |
  d2 d4 |
  d4( e) fs |
  e2 d4 |
  d4( cs) cs |
  e2 e4 |
  e2 cs4 |
  d2 d4 |
  cs2 a4 |
  b( cs) d |
  cs2 d4 |
  d2 cs4 |
  d2
}

tenor = \relative c {
  \world
  fs4 |
  a4( b) g |
  fs2 b4 |
  a2 g4 |
  fs2 fs4 |
  fs4( g) a |
  b( cs) d |
  a2 a4 |
  a2 a4 |
  cs2 gs4 |
  a2 a4 |
  a4( b) b |
  a2 a4 |
  g2 a4 |
  a( g) fs |
  b( a) g |
  fs2
}

bass = \relative c {
  \world
  d4 |
  d( g,) a |
  d2 b4 |
  fs2 g4 |
  d'2 d4 |
  d( e) fs |
  g2 fs4 |
  cs2 d4 | 
  a2 a4 |
  a4( cs) e |
  a2 g!4 |
  fs4( d) e |
  a,2 fs'4 |
  e2 d4 |
  a2 b4 |
  g4( a) a |
  d2
}

verseOne = \lyricmode {
  \set stanza = "1. "
  When I sur -- vey the won -- drous cross
  Where the young Prince of Glo -- ry died,
  My rich -- est gain I count but loss,
  And pour con -- tempt on all my pride.
}

verseTwo = \lyricmode {
  \set stanza = "2. "
  For -- bid it, Lord, that I should boast,
  Save in the cross of Christ, my God:
  All the vain things that charm me most,
  I sac -- ri -- fice them to his blood.
}

\markup {
  \column {
    \fill-line { \large \bold \title } % title
    \fill-line { \caps \piece               % piece
	         \caps \composer      % composer
	  }
    \fill-line { \meter          % meter
	         \arranger           % arranger
	  }
   }
}

\score {
  \context ChoirStaff <<
    \context Staff = upper <<
      \context Voice =
         sopranos { \voiceOne << \melody >> }
      \context Voice =
         altos { \voiceTwo << \alto >> }
      \context Lyrics = one \lyricsto sopranos \verseOne
      \context Lyrics = two \lyricsto sopranos \verseTwo
    >>
    \context Staff = lower <<
      \clef bass
      \context Voice =
        tenors { \voiceOne << \tenor >> }
      \context Voice =
        basses { \voiceTwo << \bass >> }
    >>
  >>

  \layout {
    \context {
      \Score
      % **** Turns off bar numbering
      \remove "Bar_number_engraver"
    }
    \context {
      \Lyrics
      % **** Prevents lyrics from running too close together
      \override LyricSpace #'minimum-distance = #0.6
      % **** Makes the text of lyrics a little smaller
      \override LyricText #'font-size = #-1
      % **** Moves lines of lyrics closer together
      \override VerticalAxisGroup #'minimum-Y-extent = #'(-1 . 1)
    }
  }
}

\markup { 
  \normalsize {
    \fill-line {
      \hspace #3.0
      \line {
	"3. "
	\column {
	  "See, from his head, his hands, his feet,"
	  "  Sorrow and love flow mingled down!"
	  "Did e'er such love and sorrow meet,"
	  "  Or thorns compose so rich a crown?"
	}
      }
      \hspace #3.0
      \line {
	"4. "
	\column {
	  "Were the whole realm of nature mine,"
	  "  That were an off'ring far too small;"
	  "Love so amazing, so divine,"
	  "  Demands my soul, my life, my all."
	}
      }
      \hspace #3.0
    }
  }
}

\markup {
  \fill-line {
    " "
    \column {
      \small \caps \poet % poet
    }
  }
}

%{
  Per the _Hymnal 1940 _
  Change log:
  3-24-06 Moved to 2.8, current formatting
%}


