% Version 1.1
% Last edit: March 18, 2006
% The music and words produced by this source code are believed
% to be in the public domain in the United States. The source
% code itself is covered by the Creative Commons Attribution-
% NonCommercial license, 
% http://creativecommons.org/licenses/by-nc/2.5/
% Attribution: Geoff Horton

\version "2.8.0"
\include "english.ly"

#(set-default-paper-size "letter")
#(set-global-staff-size 18)

title = "Immortal, Invisible, God Only Wise"
composer = "Welsh Melody"
poet = "Walter Chalmers Smith, alt."
piece = "St. Denio"
meter = "11 11.11 11"
arranger = ""

world = {
  \key g \major
  \time 3/4
  \partial 4*1
}

melody = \relative c'' {
  \world
  g4 |
  e c a' |
  fs d g |
  b b a |
  g2 g4 |
  e c a' |
  fs d g |
  b b a |
  g2 g4 |
  b b d |
  b g g |
  b b d |
  a2 b4 |
  g e a |
  fs d g |
  b b a |
  g2 \bar "|."
}


alto = \relative c' {
  \world
  b4 |
  c c e |
  d d b |
  d d d8[ c] |
  b2 d4 |
  c c e |
  d d b |
  d d d8[ c] |
  b2 d4 |
  g g a |
  g d b |
  d d d |
  d2 d4 |
  c c e |
  d d b |
  d d d8[ c] |
  b2
}

tenor = \relative c' {
  \world
  g4 | 
  g e c' |
  a fs g |
  g g fs |
  g2 g4 |
  g e c' |
  a fs g |
  g g fs |
  g2 b4 |
  e e d |
  d g, g |
  fs g g |
  g( fs) g |
  e g c |
  a fs g |
  g g fs |
  g2 \bar "|."
}

bass = \relative c {
  \world
  g4 |
  c c a |
  d d e |
  d b d |
  g,2 b4 |
  c c a |
  d d e |
  d b d |
  g,2 g'4 |
  e e fs |
  g b, e |
  b g b |
  d2 g,4 |
  c c a |
  d d e |
  d b d |
  g2
}

verseOne = \lyricmode {
  \set stanza = "1. "
  Im -- mor -- tal, in -- vis -- i -- ble, God on -- ly wise,
  In light in -- ac -- ces -- si -- ble hid from our eyes,
  Most bless -- ed, most glo -- rious, the An -- cient of Days,
  Al -- might -- y, vic -- to -- rious, thy great name we praise.
}

verseTwo = \lyricmode {
  \set stanza = "2. "
  Un -- rest -- ing, un -- hast -- ing, and si -- lent as light,
  Nor want -- ing, nor wast -- ing, thou ru -- lest in might;
  Thy jus -- tice like moun -- tains high soar -- ing a -- bove
  Thy clouds, which are foun -- tains of good -- ness and love.
}

\markup {
  \column {
    \fill-line { \large \bold \title } % title
    \fill-line { \caps \piece               % piece
	         \caps \composer      % composer
	  }
    \fill-line { \meter          % meter
	         \arranger           % arranger
	  }
  }
}

\score {
  \context ChoirStaff <<
    \context Staff = upper <<
      \context Voice =
         sopranos { \voiceOne << \melody >> }
      \context Voice =
         altos { \voiceTwo << \alto >> }
      \context Lyrics = one \lyricsto sopranos \verseOne
      \context Lyrics = two \lyricsto sopranos \verseTwo
    >>
    \context Staff = lower <<
      \clef bass
      \context Voice =
        tenors { \voiceOne << \tenor >> }
      \context Voice =
        basses { \voiceTwo << \bass >> }
    >>
>>
  \layout {
    \context {
      \Score
      % **** Turns off bar numbering
      \remove "Bar_number_engraver"
      \override SeparationItem #'padding = #0.6
    }
    \context {
      \Lyrics
      % **** Prevents lyrics from running too close together
      \override LyricSpace #'minimum-distance = #0.6
      % **** Makes the text of lyrics a little smaller
      \override LyricText #'font-size = #-1
      % **** Moves lines of lyrics closer together
      \override VerticalAxisGroup #'minimum-Y-extent = #'(-1 . 1)
    }
  }
}


\markup { 
  \small {
    \fill-line {
      \hspace #1.0
      \line {
	"3. "
	\column {
	  "To all life thou givest, to both great and small;"
	  "In all life thou livest, the true life of all;"
	  "We blossom and flourish, like leaves on the tree,"
	  "Then wither and perish; but naught changeth thee."
	}
      }
      \hspace #1.0
      \line {
	"4. "
	\column {
	  "Great Father of glory, pure Father of light;"
	  "Thine angels adore thee, all veiling their sight;"
	  "All laud we would render: O help us to see"
	  "’Tis only the splendor of light hideth thee."
	}
      }
      \hspace #1.0
    }
  }
}

\markup {
  \fill-line {
    " "
    \column {
      \small \caps \poet % poet
    }
  }
}

\paper {
  ragged-bottom = ##t
  top-margin = 0.25\in
  bottom-margin = 0.25\in
}

%{
  Per _The Hymnal 1940_, 301. The alterations to the original
  text are not specified in _The Hymnal Companion_, other
  than the omission of some verses.

  Change log:
  3-24-06 Move to 2.8 and new formatting
%}


